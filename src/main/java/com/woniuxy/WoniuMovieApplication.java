package com.woniuxy;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAutoConfiguration

@SpringBootApplication
@MapperScan("com.woniuxy.dao")
public class WoniuMovieApplication {

	public static void main(String[] args) {
		SpringApplication.run(WoniuMovieApplication.class, args);

	}
}
