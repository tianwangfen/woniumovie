package com.woniuxy.entity;

import java.math.BigDecimal;
import java.util.List;

import lombok.Data;
import lombok.experimental.Accessors;
@Data
@Accessors(chain = true)
public class Packages {
	private int packageId;
	private String packageName;
	private BigDecimal packagePrice;
	private List<P_fBean>  p_fBeans;

}
