package com.woniuxy.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Manager {
	private int managerId;
	private String managerAccount;
	private String managerPwd;
}
