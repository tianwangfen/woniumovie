package com.woniuxy.entity;

import java.io.Serializable;

import com.github.pagehelper.PageInfo;

import lombok.Data;
import lombok.ToString;
/**
 * 
 * @author 邹扬
 *
 */
@Data
@ToString
public class ShowVo implements Serializable{
	private String showName;
	private String[] site;
	private String siteCoordinate;
	private String flag;
	private PageInfo pageInfo;
	private String msg;
	private int code;
	
	public ShowVo() {
		super();
	}
	
	public ShowVo(String msg, int code) {
		super();
		this.msg = msg;
		this.code = code;
	}

	public final static ShowVo SUCCESS = new ShowVo("success", 200);
	public final static ShowVo FAIL = new ShowVo("fail", 500);
}
