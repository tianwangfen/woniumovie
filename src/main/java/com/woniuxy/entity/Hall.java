package com.woniuxy.entity;

import lombok.Data;

/**
 * 这是展厅（放映厅）的实体类
 * 
 * @author 余枭春
 *
 */
@Data
public class Hall {
	private Integer hallId;//厅id
	private String hallName;//展厅名字
	private String image; //展厅图片
	private Integer showId;//放映点id
	private String description;//描述信息
	private Integer cols;//列
	private Integer rows;//行
	private Show show;
}
