package com.woniuxy.entity;

import java.math.BigDecimal;

import lombok.Data;

/**
 * 这个是座位的实体类；面向前台展示数据
 * 可以通过厅id和放映点id综合查找到座位空间地址,通过排片id查找到座位的时间地址；
 * 在这儿只保存售出的作为信息；
 * 
 * @author 余枭春
 *
 */
@Data
public class SeatVO {
	private Integer hallId;// 存放厅的id
	private Integer row;// 行号
	private Integer col;// 列号
	private Integer showId;// 放映点的id
	private Integer scheduleId;// 排片id
	private String status;//未定/已定/损坏不可用
	private BigDecimal price;// 存放这场电影中的价格，由电影类中的价格和排片类中的折扣计算而得

//封存排片信息，电影票价在电影类，折扣信息在排片类中，排片类中封存有电影类
	private Schedule schedule;

}
