package com.woniuxy.entity;


import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Cinema {
	private Integer cinemaId;
	private String cinemaName;
	private String logo;
	private String equipment;
	private String c_description;
	private int packageId;

}
