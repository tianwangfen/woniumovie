package com.woniuxy.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Repository;

/**
 * @program: WoniuMovie
 * @description: 影片类别表实体类
 * @author: crowned
 * @create: 2020-06-11 18:26
 **/
@Repository
@Data
@Accessors(chain = true)
public class MovieType {
    private Integer movieTypeId; //影片类别id
    private String movieType; //影片类别
    private String remarks; //详情
}
