package com.woniuxy.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 优惠券实体类
 * 
 * @author 阳佳
 *
 */
@Data
@Accessors(chain = true)
public class Voucher {
	private Integer vid;// 主键
	private String num;// 唯一标识码
	private Integer money;// 优惠券金额 （应该有一个默认值）
	private Integer status;// 状态 过期0已使用1未使用2占用中3（付款使用优惠券时）
	private String getTime;// 获得代金券时间
	private String invalidTime;// 预计失效时间
}
