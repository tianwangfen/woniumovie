package com.woniuxy.entity;

import lombok.Data;
import lombok.experimental.Accessors;
/**
 * 封装套餐详请里面包含的功能
 * @author Administrator
 *
 */

@Data
@Accessors(chain = true)
public class P_fBean {
	private int p_f_Id;
	private int p_functionNum;//此功能的数量
	private String p_functionName;//功能名字
}
