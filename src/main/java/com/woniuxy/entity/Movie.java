package com.woniuxy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author: crowned
 * @Date: 2020/6/9 22:42
 * @Description: 影片表实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Movie {
    private Integer movieId; //影片id
    private String movieNo; //影片编号
    private String movieName;//影片名
    private String director;//导演
    private String description;//影片描述
    private BigDecimal price;//价格
    private String releaseTime;//上映时间
    private String soldOutTime;//下架时间
    private String image;//影片宣传图
    private Integer movieStatus;//电影状态 1上映中，0待上映，2下架
    private Integer movieTypeId; //影片类别
    private List<Actor> actors;//演员
    private Integer length;//时长
    private int pageNum;
    private int pageSize;
}
