package com.woniuxy.entity;

import java.io.Serializable;

import lombok.Data;
import lombok.ToString;
/**
 * 
 * @author 邹扬
 *
 */
@Data
@ToString
public class ShowDto implements Serializable {
	private Integer id;
	private String showName;
	private String site;
	private String siteCoordinate;
	private String flag;

	private Integer pageNum;
	private Integer pageSize;

}
