package com.woniuxy.entity;

import java.math.BigDecimal;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class OrderDetails {
	//座位
	private String col;//列
	private String row;//行
	//电影
	private String movieName;//电影名
	//订单
	private Integer orderId;
	private String orderNo;//订单号
	private BigDecimal price;//单价
	
	private Integer detailId;
	
	private String place;//影院名-放映点名-放映厅
	
}
