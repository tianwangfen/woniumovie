package com.woniuxy.entity;

import java.util.List;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PageBean<E> {
	private List<E> data;//数据
	private int totalNum;//总数
	private int totalPage;//总页数
	private int pageSize;//每页的数量
	private int page;//当前页码
	private int CurrentPage;//当前页码
	
}
