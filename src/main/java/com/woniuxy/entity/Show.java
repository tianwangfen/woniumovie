package com.woniuxy.entity;

import java.io.Serializable;

import org.apache.http.entity.SerializableEntity;

import lombok.Data;
import lombok.experimental.Accessors;
/**
 * 
 * @author 邹扬
 *
 */
@Data
@Accessors(chain = true)
public class Show implements Serializable {
	private Integer showId;
	private String showName;
	private String province;
	private String city;
	private String area;
	private String site;
	private String siteCoordinate;
	private String description;
	private String image;
	private String flag;
	private Integer cinemaId;
}
