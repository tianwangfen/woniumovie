package com.woniuxy.entity;

import java.math.BigDecimal;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 订单类，支持订单明细
 * 
 * @author 余枭春
 *
 */
@Data
@Accessors(chain = true)
public class OrderDTO {
	// 订单
	private Integer id;// 通过订单id直接获取，更改订单状态

	private Integer userId;// 通过用户id查询订单
	private String orderNo;
	private String telphone;// 用户电话，订单展示用
	private BigDecimal totalMoney;// 订单总金额
	private String payType;// 支付方式
	private String payTime;// 支付时间

	private String ordertime;// 下单时间，条件范围查询用
	private String status;// 订单状态，条件查询用
//用户
	private UserPO user;
//订单明细
	private int nums;// 票的张数
	// 前端返回的订票数据
	private String seatsInfo;// 这种字符串"A-10,A-11"

	private String col;// 列
	private String row;// 行
	// 电影
	private String movieName;// 电影名
	private BigDecimal price;// 单价

	private Integer detailId;
	private Integer movieId;
	private Integer showId;
	private Integer hallId;
//	private Integer cinimalId;

	private String place;// 影院名-放映点名-放映厅

}
