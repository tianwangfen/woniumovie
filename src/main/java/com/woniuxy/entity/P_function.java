package com.woniuxy.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class P_function {
	private int p_functionId;
	private String p_functionName;
}
