//package com.woniuxy.entity;
//
//import java.math.BigDecimal;
//
//import lombok.Data;
//
///**
// * 排片实体类封装页面查询条件
// * 
// * @author 余枭春
// *
// */
//@Data
//public class ScheduleDTO {
//	private Integer scheduleId;//排片id
//	private Integer showId;//放映点id
//	private Integer movieId;//电影名
//	private Integer hallId;//展厅id
//	private String beginTime;//开场时间
//	private String endTime;//最后开场时间
//	private BigDecimal discount;//折扣
//	private Integer movieSessionId;//电影场次id
//	
//	private Movie movie;//封装电影信息
//	private Hall hall;//封装展厅信息
//	private Show show;//封装放映点信息
//
//}
