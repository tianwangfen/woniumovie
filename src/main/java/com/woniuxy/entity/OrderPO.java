package com.woniuxy.entity;

import java.math.BigDecimal;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 
 * @author 余枭春
 *
 */
@Data
@Accessors(chain = true)
public class OrderPO {
	private Integer id;
	private String orderNo;
	private Integer userId;//用户id
	private String telphone;
	private String ordertime;
	private BigDecimal totalMoney;
	private String payType;
	private String payTime;
	private String status;//0未付款，1已付款未开场，2已付款已观影，3已付款未观影，4订单失效
}
