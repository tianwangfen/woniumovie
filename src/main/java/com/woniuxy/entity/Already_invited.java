package com.woniuxy.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 用来存储已经邀请过的用户
 * 
 * @author 阳佳
 *
 */
@Data
@Accessors(chain = true)
public class Already_invited {
	private Integer aid;// 主键
	private String email;// 被邀请人邮件
	private Integer uid;// 邀请人
}
