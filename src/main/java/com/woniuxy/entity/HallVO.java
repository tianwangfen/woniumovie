package com.woniuxy.entity;

import java.util.List;

import lombok.Data;

/**
 * 这是展厅（放映厅）的实体类
 * 
 * @author 余枭春
 *
 */
@Data
public class HallVO {
	private Integer hallId;//厅id
	private String hallName;//展厅名字
	private String image; //展厅图片
	private Integer showId;//放映点id
	private String description;//描述信息
	
	private List<Show> shows;//存放该影院所有的放映点
}
