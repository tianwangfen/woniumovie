package com.woniuxy.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 平台用户实体类 
 * @author 阳佳
 *
 */
@Data
public class UserPO {
	private Integer uid; //用户id
	private String account; // 账号
	private String password; // 密码
	private String phoneNum; //用户手机号
	private String email; // 邮箱
	private String gender; // 性别
	private String birthdate; // 生日
	private String address; // 地址
	private Integer status; //状态 0注销 1正常使用中
	private String headImage; //用户头像

}
