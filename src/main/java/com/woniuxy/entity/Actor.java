package com.woniuxy.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Repository;

/**
 * @program: WoniuMovie
 * @description: 演员表实体类
 * @author: crowned
 * @create: 2020-06-11 18:23
 **/
@Repository
@Data
@Accessors(chain = true)
public class Actor {
    private Integer actorId; //演员id
    private String actorName; //演员名
    private String detailed;//详情
}
