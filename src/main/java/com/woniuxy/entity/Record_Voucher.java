package com.woniuxy.entity;

import java.util.List;

import lombok.Data;
import lombok.experimental.Accessors;
/**
 * 
 * @author 阳佳
 *
 */
@Data
@Accessors(chain = true)
public class Record_Voucher {
	private Integer rid; // 主键
	private Integer uid; // 用户id
	private int vid; // 代金券id
	private String getTime;// 获得代金券时间
	private String useTime;// 使用代金券时间
	private int status;// 状态过期0已使用1未使用2占用中3（付款使用优惠券时）
	private String email;// 被邀请人邮箱
}
