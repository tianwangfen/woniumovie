package com.woniuxy.entity;

import java.math.BigDecimal;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 
 * @author 余枭春
 *
 */
@Data
@Accessors(chain = true)
public class OrderVO {
	private Integer id;//通过订单id直接获取，更改订单状态

	private Integer userId;//通过用户id查询订单
	
	private String telphone;//用户电话，订单展示用
	private BigDecimal totalMoney;//订单总金额
	private String payType;//支付方式
	private String payTime;//支付时间

	private String ordertime;//下单时间，条件范围查询用
	private String status;//订单状态，条件查询用
	
	private UserPO user;
}
