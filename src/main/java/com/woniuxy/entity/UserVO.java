package com.woniuxy.entity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 用于封装前端传入的数据 不与数据库进行交互
 * 
 * @author 阳佳
 *
 */
@Data
public class UserVO {
	@NotBlank(message = "账号不能为空")
	private String account; // 账号
	private String password; // 密码
	private String repassword; // 确认密码
	private String phoneNum; // 用户手机号
	private String code;// 前端传入的验证码
	private String email; // 邮箱
	private String gender; // 性别
	private String birthdate; // 生日
	private String address; // 地址
	private String headImage; // 用户头像

}
