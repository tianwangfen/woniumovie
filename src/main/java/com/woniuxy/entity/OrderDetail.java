package com.woniuxy.entity;

import java.math.BigDecimal;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
public class OrderDetail {
	private Integer detailId;
	private Integer orderId;
	private Integer movieId;
	private Integer nums;
	private BigDecimal price;
	
}
