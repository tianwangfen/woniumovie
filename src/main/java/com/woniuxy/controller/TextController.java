package com.woniuxy.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.woniuxy.service.TextService;
import com.woniuxy.utils.CodeUtils;
import com.woniuxy.utils.RedisUtil;
/**
 * 
 * @author 阳佳
 *
 */
@Controller
@RequestMapping("/text")
public class TextController {
	@Autowired
	private TextService textService;
	@Autowired
	private RedisUtil redisUtil;

	@RequestMapping("/send")
	@ResponseBody
	public void send(String phoneNum) {
		// 1.得到验证码
		int code = CodeUtils.getCode();
		// 2. 发送验证码
		try {
			textService.send(phoneNum, "1", new String[] { String.valueOf(code), "2" });//数字1表示发送短信采用的模板 数字2表示在多少时间内过期
		} catch (Exception e) {
			// TODO 具体异常?
			e.printStackTrace();
		}
		// 3. 将验证码缓存在redis中
		redisUtil.set("phoneNum"+phoneNum, code,120);		
		System.out.println(String.valueOf(redisUtil.get("phoneNum"+phoneNum)));//test
	}

}
