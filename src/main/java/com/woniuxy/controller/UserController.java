package com.woniuxy.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.woniuxy.entity.UserPO;
import com.woniuxy.entity.UserVO;
import com.woniuxy.service.UserService;
import com.woniuxy.utils.ErrorNumManager;
import com.woniuxy.utils.RedisUtil;
import com.woniuxy.utils.ResponseEntity;
/**
 * 
 * @author 阳佳
 *
 */
@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService userService;
	@Autowired
	private RedisUtil redisUtil;
	@Autowired
	private ErrorNumManager errorNumManager;

	/**
	 * 注册账号
	 * 
	 * @param userVO
	 * @return
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	@SuppressWarnings("static-access")
	@RequestMapping("/register")
	@ResponseBody
	public ResponseEntity<String> insertUser(UserVO userVO) { // 前端传入的user
		// result 用来接收校验结果
		//if (result.hasErrors()) {
			// 获取所有的错误对象信息
//			List<FieldError> errors = result.getFieldErrors();
//			Iterator<FieldError> iterator = errors.iterator();
//			while (iterator.hasNext()) {
//				FieldError fieldError = (FieldError) iterator.next();
//				// 获取错误信息
//				return new ResponseEntity<String>(33, fieldError.getDefaultMessage());
//			}
//		}
		// 先判断 手机号 账号是否已存在
		if (userService.findUserPOByAccount(userVO.getAccount()) != null) {
			return new ResponseEntity<String>(11, "账号已存在");
		}
		if (userService.findUserPOByPhoneNum(userVO.getPhoneNum()) != null) {
			return new ResponseEntity<String>(22, "手机号已注册");
		}
		// 判断两次密码是否一致
		if (!userVO.getPassword().equals(userVO.getRepassword())) {
			return new ResponseEntity<String>(1, "两次密码不一致");
		}
		// 获取用户手机号 读取redis中对应的值
		String value = String.valueOf(redisUtil.get("phoneNum" + userVO.getPhoneNum()));
		// redis中查到的值与前端输入的验证码与redis中的一致 则执行注册操作
		if (!userVO.getCode().equals(value)) {
			return new ResponseEntity<String>(2, "验证码错误");
		}
		// 创建空的UserPO对象
		UserPO userPO = new UserPO();
		// 将VO的值赋值给PO
		try {
			BeanUtils.copyProperties(userPO, userVO);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		// 将密码加密
		String pwdAfter = new SimpleHash("MD5", userPO.getPassword(), userPO.getAccount(), 1024).toString();
		userPO.setPassword(pwdAfter);
		// 将用户状态设置为1 正常使用
		userPO.setStatus(1);
		userService.insertUser(userPO);
		return new ResponseEntity<String>(200, "注册成功");
	}

	/**
	 * 方法功能：根据账号和密码登录 密码错误三次 则要输入图形验证码
	 * 
	 * @return
	 */

	@RequestMapping("/loginByAccount")
	@ResponseBody
	public ResponseEntity<String> loginByAccount(UserVO userVO) {
		// 整体逻辑：每次校验前 先校验密码错误次数 如果错了三次及以上 就校验验证码 登录成功后 将用户信息存在session中
		Subject currentUser = SecurityUtils.getSubject();
		if (!currentUser.isAuthenticated()) {
			String pwdAfter = new SimpleHash("MD5", userVO.getPassword(), userVO.getAccount(), 1024).toString();
			userVO.setPassword(pwdAfter);
			UsernamePasswordToken token = new UsernamePasswordToken(userVO.getAccount(), userVO.getPassword());
			// 得到当前账号密码错误次数
			int currErrorNum = errorNumManager.currErrorNum(userVO.getAccount());
			try {
				// 密码错误＞=3 则校验验证码
				if (errorNumManager.reachErrorMax(currErrorNum)) {
					String captcha = (String) redisUtil.get("captcha");
					if (captcha.equalsIgnoreCase(userVO.getCode())) {
						currentUser.login(token);
						// 将用户信息存在session中
						Session session = currentUser.getSession();
						session.setAttribute("user", userService.findUserPOByAccount(userVO.getAccount()));
						// 登录成功 则将密码错误次数从redis缓存中删除
						redisUtil.del("errorCount:" + userVO.getAccount());
					} else {
						// 因为redis中密码两分钟过期 第二次操作时 间隔时间可能不到两分钟 要先将redis中的验证码删除
						redisUtil.del("captcha");
						return new ResponseEntity<String>(4, "验证码错误 请重新输入");
					}

				} else {
					currentUser.login(token);
					// 将用户信息存在session中
					Session session = currentUser.getSession();
					session.setAttribute("user", userService.findUserPOByAccount(userVO.getAccount()));
					// 登录成功 则将密码错误次数从redis缓存中删除
					redisUtil.del("errorCount:" + userVO.getAccount());
				}
			} catch (UnknownAccountException e) {
				return new ResponseEntity<String>(111, "账号不存在");
			} catch (IncorrectCredentialsException e) {
				// 密码错误次数+1
				errorNumManager.addErrorNum(userVO.getAccount());
				// 根密码错误次数 返回不同数据
				if (errorNumManager.reachErrorMax(errorNumManager.currErrorNum(userVO.getAccount()))) {
					return new ResponseEntity<String>(3, "密码错误 请输入验证码");
				}
				return new ResponseEntity<String>(112, "密码错误");
			} catch (AuthenticationException e) {
				return new ResponseEntity<String>(113, "系统维护 请稍后再试");
			}
		}
		return new ResponseEntity<String>(200, "登陆成功");
	}

	@RequestMapping("/loginByPhoneNum")
	@ResponseBody
	public ResponseEntity<String> loginByPhoneNum(UserVO userVO) {
		// 整体逻辑 判断手机号是否存在 再判断验证码是否正确
		if (userService.findUserPOByPhoneNum(userVO.getPhoneNum()) == null) {
			return new ResponseEntity<String>(1, "该手机号不存在");
		}
		String value = String.valueOf(redisUtil.get("phoneNum" + userVO.getPhoneNum()));
		if (!userVO.getCode().equals(value)) {
			// 因为redis中密码两分钟过期 第二次操作时 间隔时间可能不到两分钟 要先将redis中的验证码删除
			redisUtil.del("phoneNum" + userVO.getPhoneNum());
			return new ResponseEntity<String>(2, "验证码错误");
		}
		// 将用户信息存在session中
		Subject currentUser = SecurityUtils.getSubject();
		Session session = currentUser.getSession();
		session.setAttribute("user", userService.findUserPOByPhoneNum(userVO.getPhoneNum()));
		return new ResponseEntity<String>(200, "登陆成功");
	}

	@RequestMapping("/resetPasswordFirst")
	@ResponseBody
	public ResponseEntity<String> resetPasswordFirst(UserVO userVO) {
		// 整体逻辑 判断手机号是否存在 再判断验证码是否正确
		if (userService.findUserPOByPhoneNum(userVO.getPhoneNum()) == null) {
			return new ResponseEntity<String>(1, "该手机号不存在");
		}
		String value = String.valueOf(redisUtil.get("phoneNum" + userVO.getPhoneNum()));
		if (!userVO.getCode().equals(value)) {
			// 因为redis中密码两分钟过期 第二次操作时 间隔时间可能不到两分钟 要先将redis中的验证码删除
			redisUtil.del("phoneNum" + userVO.getPhoneNum());
			return new ResponseEntity<String>(2, "验证码错误");
		}

		return new ResponseEntity<String>(200, "登陆成功");
	}

	@RequestMapping("/resetPasswordSecond")
	@ResponseBody
	public ResponseEntity<String> resetPasswordSecond(UserVO userVO) {
		// 整体逻辑 判断输入框的验证码是否一致
		if (!userVO.getPassword().equals(userVO.getRepassword())) {
			return new ResponseEntity<String>(1, "两次密码不一致");
		}
		// 通过手机号查询到该账户
		UserPO user = userService.findUserPOByPhoneNum(userVO.getPhoneNum());
		// 将前端输入密码加密
		String pwdAfter = new SimpleHash("MD5", userVO.getPassword(), user.getAccount(), 1024).toString();
		userService.resetPassword(pwdAfter, user.getPhoneNum());
		return new ResponseEntity<String>(200, "成功");
	}

	@RequestMapping("/editProfile")
	@ResponseBody 
	public ResponseEntity<String> editProfile(UserVO userVO) throws IllegalAccessException, InvocationTargetException {
		Subject currentUser = SecurityUtils.getSubject();
		Session session = currentUser.getSession();
		UserPO userPO = (UserPO) session.getAttribute("user");
		BeanUtils.copyProperties(userPO, userVO);
		userService.updateUserInfoByUid(userPO, userPO.getUid());
		return new ResponseEntity<String>(200, "成功");
	}
	@RequestMapping("/find")
	@ResponseBody
	public UserPO  find() {
		Subject currentUser = SecurityUtils.getSubject();
		Session session = currentUser.getSession();
		UserPO userPO = (UserPO) session.getAttribute("user");
		int uid=userPO.getUid();
		return userService.findUserPOByUid(uid);
	}
}