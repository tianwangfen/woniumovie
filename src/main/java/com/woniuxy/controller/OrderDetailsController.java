package com.woniuxy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.woniuxy.entity.OrderDetails;
import com.woniuxy.service.OrderDetailsService;
import com.woniuxy.utils.ResponseEntity;

/**
 * 订单明细，票的信息也从这里直接拿
 * 
 * @author 余枭春
 *
 */
@Controller
@RequestMapping("/orderDetails")
public class OrderDetailsController {

	@Autowired
	private OrderDetailsService orderDetailsService;

	@RequestMapping(value = "/list/{orderId}", method = RequestMethod.GET)
	public ResponseEntity<List<OrderDetails>> listDetailsByOrderId(@PathVariable Integer orderId) {
		try {
			List<OrderDetails> res = orderDetailsService.listDetailsByOrderId(orderId);
			return new ResponseEntity<List<OrderDetails>>(res);
		} catch (Exception e) {
			return new ResponseEntity<List<OrderDetails>>(500, "服务器拒绝了宁德请求，您是否从来没在我们这下过单呀~");
		}
	}

}
