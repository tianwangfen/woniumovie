package com.woniuxy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.woniuxy.entity.Hall;
import com.woniuxy.entity.Schedule;
import com.woniuxy.service.HallService;
import com.woniuxy.service.ScheduleService;
import com.woniuxy.utils.RedisUtil;
import com.woniuxy.utils.ResponseEntity;
import com.woniuxy.utils.ScheduleUtils;

/**
 * 排片业务控制类
 * 
 * @author 余枭春
 *
 */
@Controller
@RequestMapping("/schedule")
public class ScheduleController {
	@Autowired
	private ScheduleService scheduleService;
	@Autowired
	private HallService hallService;
	@Autowired
	private RedisUtil redisUtil;

	/**
	 * 新增排片信息
	 * 
	 * @param schedule 排片信息
	 * @return 修改成功200，否者500
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Integer> insertSchedule(Schedule schedule) {
		// TODO 在创建排片的时候创建一个二维数据[行[列，状态(0可用,1已售,2禁用)，是否加价(0不加,1一级,2二级)][]]
		int result = scheduleService.insertSchedule(schedule);
		if (result > 0) {
			Hall hall = hallService.getHallByHallId(schedule.getHallId());
			// 将座位信息存放在一个二维数组中，并存放在redis中
			String keyOfSeats = ScheduleUtils.getKeyOfSeats(schedule);
			String[][] seats = new String[hall.getRows()][hall.getCols()];
			redisUtil.set(keyOfSeats, seats);
			return new ResponseEntity<Integer>(200, "ok", result);
		} else {
			return new ResponseEntity<Integer>(500, "添加失败，系统正在维护", null);
		}
	}

	/**
	 * 修改拍片信息
	 * 
	 * @param schedule 封装所有的拍片信息，前端传回来完整的信息
	 * @return 修改成功200，否者500
	 */
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Integer> updateSchedule(Schedule schedule) {
		int result = scheduleService.updateSchedule(schedule);
		if (result > 0) {
			return new ResponseEntity<Integer>(200, "ok", null);
		} else {
			return new ResponseEntity<Integer>(500, "修改失败，系统正在维护", null);
		}
	}

	/**
	 * 根据查询条件返回查询到的排片信息
	 * 
	 * @param schedule
	 * @return
	 */
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<List<Schedule>> listSchedule(/*@PathVariable Integer showId,@PathVariable Integer movieId,@PathVariable String beginDate*/Schedule schedule) {
//		Schedule schedule=new Schedule();
//		schedule.setBeginDate(beginDate);
//		schedule.setMovieId(movieId);
//		schedule.setShowId(showId);
		System.out.println("============="+schedule.getBeginDate()+schedule.getShowId()+schedule.getMovieId());
		try {
			List<Schedule> result = scheduleService.listSchedule(schedule);
			return new ResponseEntity<List<Schedule>>(result);
		} catch (Exception e) {
			return new ResponseEntity<List<Schedule>>(500, "查询失败，系统正在维护", null);
		}
	}

	/**
	 * 获取单个排片信息
	 * 
	 * @param scheduleId
	 * @return
	 */
	@RequestMapping(value = "/get/{scheduleId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Schedule> getSchedule(@PathVariable Integer scheduleId) {
		try {
			Schedule result = scheduleService.getSchedule(scheduleId);
//			String keyOfSeats = ScheduleUtils.getKeyOfSeats(result);
//			String[][] seats = (String[][]) redisUtil.get(keyOfSeats);
//			result.setSeats(seats);
			return new ResponseEntity<Schedule>(result);
		} catch (Exception e) {
			return new ResponseEntity<Schedule>(500, "查询失败，系统正在维护", null);
		}
	}

	/**
	 * 删除排片信息
	 * 
	 * @param schedule
	 * @return
	 */
	@RequestMapping(value = "/del/{scheduleId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseEntity<Integer> deleteSchedule(@PathVariable Integer scheduleId) {
		System.out.println(scheduleId);
		// TODO 查询售票信息，是否有售票
		boolean s = false;
		if (s) {// 如果有售出去的票
			return new ResponseEntity<Integer>(500, "删除失败，该场已经售出了电影票", null);
		} else {
			// 如果没有售出票，就可以删除
			int result = scheduleService.deleteSchedule(scheduleId);
			if (result > 0) {
				return new ResponseEntity<Integer>(200, "ok", null);
			} else {
				return new ResponseEntity<Integer>(500, "修改失败，系统正在维护", null);
			}
		}

	}

	/**
	 * 查询该电影已排的最新场次
	 * 
	 * @param movieId
	 * @return
	 */
	@RequestMapping(value = "/getSession/{movieId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Integer> getMaxMovieSessionIdByMovieId(@PathVariable Integer movieId) {
		try {
			Integer result = scheduleService.getMaxMovieSessionIdByMovieId(movieId);
			return new ResponseEntity<Integer>(200, "ok", result);
		} catch (Exception e) {
			return new ResponseEntity<Integer>(500, "修改失败，系统正在维护", null);
		}

	}

	@RequestMapping(value = "/getlastEndTime/{beginDate}/{hallId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> getLastEndTimeWithBeginDate(@PathVariable String beginDate,@PathVariable Integer hallId) {
		try {
			String res = scheduleService.getLastEndTimeWithBeginDate(beginDate,hallId);
			if (res != null) {
				return new ResponseEntity<String>(res);
			} else {
				return new ResponseEntity<String>("00:00:00");
			}
		} catch (Exception e) {
			return new ResponseEntity<String>(500, "查询失败，系统正在维护", null);
		}
	}
	
	@RequestMapping(value = "/getExlastEndTime/{beginDate}/{hallId}/{beginTime}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> getExLastEndTimeWithBeginDate(@PathVariable String beginDate,@PathVariable Integer hallId,@PathVariable String beginTime) {
		try {
			String res = scheduleService.getExLastEndTimeWithBeginDate(beginDate,hallId,beginTime);
			if (res != null) {
				return new ResponseEntity<String>(res);
			} else {
				return new ResponseEntity<String>("00:00:00");
			}
		} catch (Exception e) {
			return new ResponseEntity<String>(500, "查询失败，系统正在维护", null);
		}
	}

}
