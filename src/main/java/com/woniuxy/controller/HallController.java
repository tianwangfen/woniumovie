package com.woniuxy.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.woniuxy.entity.Hall;
import com.woniuxy.service.HallService;
import com.woniuxy.utils.ResponseEntity;

/**
 * 厅的业务控制层
 * 
 * @author 余枭春
 *
 */
@RequestMapping("/hall")
@Controller
public class HallController {

	@Autowired
	private HallService hallService;

	/**
	 * 增加厅的信息
	 * 
	 * @param hall 将所有的信息封装到hall中
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Hall> insertHall(Hall hall) {
		int result = hallService.insertHall(hall);
		if (result > 0) {
			try {
				Hall resHall = hallService.getHallByShowIdAndName(hall.getShowId(), hall.getHallName());
				return new ResponseEntity<Hall>(200, "ok", resHall);
			} catch (Exception e) {
				return new ResponseEntity<Hall>(500, "添加失败，系统正在维护");
			}
		} else {
			return new ResponseEntity<Hall>(500, "添加失败，系统正在维护", null);
		}
	}

	/**
	 * 修改厅的信息，hall中封装所有厅的信息
	 * 
	 * @param hall
	 * @return
	 */
	// TODO 后期是否需要先判断条件是否为空，先查出原来的，再改相对的条件
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Integer> updateHall(Hall hall) {
		int result = hallService.updateHall(hall);
		if (result > 0) {
			return new ResponseEntity<Integer>(200, "ok", result);
		} else {
			return new ResponseEntity<Integer>(500, "更新失败，系统正在维护", null);
		}
	}

	/**
	 * 通过id删除厅
	 * 
	 * @param hallId
	 * @return
	 */
	@RequestMapping(value = "/del/{hallId}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseEntity<Integer> deleteHallByHallId(@PathVariable Integer hallId) {
		int result = hallService.deleteHallByHallId(hallId);
		if (result > 0) {
			return new ResponseEntity<Integer>(200, "ok", result);
		} else {
			return new ResponseEntity<Integer>(500, "删除失败，系统正在维护", null);
		}
	}

	/**
	 * 查询所有的厅信息，可以按照条件查询
	 * 
	 * @param hall 封装查询条件
	 * @return
	 */
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<List<Hall>> listHall(Hall hall) {
		try {
			List<Hall> result = hallService.listHall(hall);
			return new ResponseEntity<List<Hall>>(200, "ok", result);
		} catch (Exception e) {
			return new ResponseEntity<List<Hall>>(500, "删除失败，系统正在维护", null);
		}
	}

	/**
	 * 通过id查询对应的厅信息
	 * 
	 * @param hallId
	 * @return
	 */
	@RequestMapping(value = "/get/{hallId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Hall> getHallByHallId(@PathVariable Integer hallId) {
		try {
			Hall result = hallService.getHallByHallId(hallId);
			return new ResponseEntity<Hall>(200, "ok", result);
		} catch (Exception e) {
			return new ResponseEntity<Hall>(500, "删除失败，系统正在维护", null);
		}
	}

	/**
	 * 通过放映点id获取放映厅
	 * 
	 * @param showId
	 * @return
	 */
	@RequestMapping(value = "/find/{showId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<Hall>> getHallsByShowId(@PathVariable Integer showId) {
		try {
			List<Hall> result = hallService.getHallsByShowId(showId);
			return new ResponseEntity<List<Hall>>(200, "ok", result);
		} catch (Exception e) {
			return new ResponseEntity<List<Hall>>(500, "获取失败，系统正在维护", null);
		}
	}

	/**
	 * 上传文件，返回文件路径
	 * 
	 * @param file 上传的文件
	 * @param req  请求
	 * @return 返回文件存储路径
	 */
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> upload(MultipartFile file, HttpServletRequest req) {
		// 获取文件的原始名称
		String oldFileName = file.getOriginalFilename();

		// 获取文件后缀
		String extension = FilenameUtils.getExtension(file.getOriginalFilename());

		// 生成文件新名称
		String newFileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())
				+ UUID.randomUUID().toString().replace("-", "") + "." + extension;

		// 文件的大小
		long size = file.getSize();

		// 文件类型
		String type = file.getContentType();
		String realPath = "";
		realPath += req.getServletContext().getRealPath("") + "images" + File.separator + "hallImg";

		// 处理根据日期生成目录
		String dateformat = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		String dateDirPath = realPath + File.separator + dateformat;
		File dateDir = new File(dateDirPath);
		if (!dateDir.exists()) {
			dateDir.mkdirs();
		}
		String imgPath = "";
		// 处理文件上传
		try {
			file.transferTo(new File(dateDir, newFileName));
			imgPath = "\\images" + File.separator + "hallImg" + File.separator + dateformat + File.separator
					+ newFileName;
		} catch (IllegalStateException | IOException e) {
			return new ResponseEntity<String>("上传失败哦");
		}

		return new ResponseEntity<String>(imgPath);
	}
}
