package com.woniuxy.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.woniuxy.service.impl.PicUploadServiceImpl;

/**
 * 图片上传
 * @author 阳佳
 *
 */
@RequestMapping("/pic")
@Controller
public class PicController {
	@Autowired
	private PicUploadServiceImpl picUploadService;
	@RequestMapping("/upload")
	@ResponseBody
	public void upload(@RequestParam("file") MultipartFile multipartFile,HttpServletRequest request) {
		picUploadService.upLoad(multipartFile,request);
	}
}
