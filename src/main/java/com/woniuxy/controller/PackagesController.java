package com.woniuxy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.woniuxy.entity.Cinema;
import com.woniuxy.entity.Packages;
import com.woniuxy.service.CinemaService;
import com.woniuxy.service.P_fService;
import com.woniuxy.service.PackagesService;
import com.woniuxy.utils.ResponseEntity;

@Controller
@RequestMapping("/packages")
public class PackagesController {
	@Autowired
	PackagesService packagesService;
	
	@Autowired
	CinemaService cinemaService;
	
	@Autowired 
	P_fService p_fService;
	
	@RequestMapping("/listAll")
	@ResponseBody
	public List<Packages> listAll(){
		return packagesService.listAll();
	}
	
	@DeleteMapping("/del/{packageId}")
	@ResponseBody
	public ResponseEntity<Integer> del(@PathVariable int packageId) {
		//先判断影院是否有该套餐
		Cinema cinema = cinemaService.selectCinemaBypackageId(packageId);
		System.out.println(cinema);
		if(cinema == null) {
			//没有则先删除套餐功能表的数据 再删除套餐表的数据
			int res = p_fService.del(packageId);
			if(res>0) {
				//删除套餐表数据
				int  res1 = packagesService.del(packageId);
				if(res1>0) {
					return new ResponseEntity<Integer>(200, "删除成功", null);
				}else {
					return new ResponseEntity<Integer>(500, "删除失败", null);
				}
			}else {
				return new ResponseEntity<Integer>(500, "删除失败", null);
			}
		}else {
			return new ResponseEntity<Integer>(500, "操作失败", null);
		}
	}
	
	public ResponseEntity<Integer> change(int packageId){
		Cinema cinema = cinemaService.selectCinemaBypackageId(packageId);
		if(cinema==null) {
			//对套餐的修改，直接修改套餐功能表的数据
			
		}else {
			return new ResponseEntity<Integer>(500, "无法修改", null);
		}
		return null;
	}
}
