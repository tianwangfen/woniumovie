package com.woniuxy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.woniuxy.entity.Cinema;
import com.woniuxy.service.CinemaService;
import com.woniuxy.utils.ResponseEntity;

@Controller
@RequestMapping("/cinema")
public class CinemaController {
	
	@Autowired
	private CinemaService cinemaService;
	
	@RequestMapping("/all")
	@ResponseBody
	public ResponseEntity listAll(){
		
		List<Cinema> cinemas = cinemaService.listAll();
		if(cinemas==null&&cinemas.isEmpty()) {
			return new ResponseEntity<Void>().FAIL;
		}else {
			return new ResponseEntity<List<Cinema>> (cinemas);
			
		}
	}
	
	
		
	
}
