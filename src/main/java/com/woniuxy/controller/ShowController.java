package com.woniuxy.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.web.multipart.MultipartFile;

import com.woniuxy.entity.Show;
import com.woniuxy.entity.ShowDto;
import com.woniuxy.entity.ShowVo;
import com.woniuxy.service.ShowService;
import com.woniuxy.utils.ResponseEntity;
/**
 * 
 * @author 邹扬
 *
 */
@Controller
@RequestMapping("/show")
public class ShowController {

	@Autowired
	private ShowService showService;

	@RequestMapping("/all")
	@ResponseBody
	public ShowVo listAll() {
		return showService.listAll();
	}

	@RequestMapping("/list")
	@ResponseBody
	public ShowVo listByCondition(ShowDto showDto) {
		System.out.println(showDto.toString());
		try {
			ShowVo showVo = showService.ListByCondition(showDto);
			showVo.setCode(200);
			showVo.setMsg("success");
			return showVo;
		} catch (Exception e) {
			e.printStackTrace();
			return ShowVo.FAIL;
		}

	}

	@PostMapping("/add")
	@ResponseBody
	public ResponseEntity insertShow(Show show) {
		System.out.println(show);
		int res = showService.insertShow(show);
		ResponseEntity<Void> responseEntity = new ResponseEntity<>();
		if(res==1) {
			return responseEntity.SUCCESS;
		}else {
			return responseEntity.FAIL;
		}
	}

	@RequestMapping("/delete")
	@ResponseBody
	public ResponseEntity<Void> batchDeleteShows(@RequestBody Integer[] ids) {
		List<Integer> idList = new ArrayList<>();
		for (Integer id : ids) {
			idList.add(id);
		}
		int res = showService.batchDeleteShows(idList);
		if (res > 0) {

			return ResponseEntity.SUCCESS;
		} else {
			return ResponseEntity.FAIL;
		}
	}

	@RequestMapping(value = "/update")
	@ResponseBody
	public ResponseEntity<Void> updateShow(Show show) {
		int res = showService.updateShow(show);
		if (res > 0) {
			return ResponseEntity.SUCCESS;

		} else {
			return ResponseEntity.FAIL;
		}
	}

	@RequestMapping(value = "/qeury/{showId}", method = RequestMethod.GET)
	@ResponseBody

	public ResponseEntity<Show> queryShowByShowId(@PathVariable Integer showId) {
		Show show = showService.getShowById(showId);
		return new ResponseEntity<>(show);
	}
	
	@RequestMapping(value = "queryCinemaShows")
	@ResponseBody
	public ResponseEntity queryShowByCinemaId(Integer cid) {
		System.out.println(cid);
		try {
			return new ResponseEntity<List<Show>>(showService.getShowByCinemaId(cid));
		} catch (Exception e) {
			return new ResponseEntity<>(500, "查询失败");
		}
	}

	@RequestMapping("/listAll")
	@ResponseBody
	public ResponseEntity<List<Show>> listALL() {
		try {
			return new ResponseEntity<List<Show>>(showService.listALL());
		} catch (Exception e) {
			return new ResponseEntity<List<Show>>(500, "系统繁忙");
		}
	}

	@PostMapping("/upload")
	@ResponseBody
	public String upload(MultipartFile file,HttpServletRequest request) {
		//获取文件的原始名称
		String oldFileName=file.getOriginalFilename();
		
		//获取文件后缀
		String extension = FilenameUtils.getExtension(oldFileName);
		
		//生成文件新名称
		String newFileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())
				+ UUID.randomUUID().toString().replace("-", "")+extension;
		System.out.println(newFileName);
		
		//文件的大小
		long size = file.getSize();
		
		//文件类型
		String type = file.getContentType();
		
		
		//处理文件上传
		//ResourceUtils.getURL("classpath:")
		//D:/eclipse/eclipese-workplace/woniumovie/target/classes/
		//request.getServletContext().getRealPath("")
		//D:\eclipse\eclipese-workplace\woniumovie\src\main\webapp\

		String realPath = "";
		realPath+=request.getServletContext().getRealPath("")+"images"+File.separator+"showImg";
		System.out.println(realPath);
		
		//处理根据日期生成目录
		String dateformat = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		String dateDirPath = realPath+File.separator+dateformat;
		File dateDir = new File(dateDirPath);
		if(!dateDir.exists()) {
			dateDir.mkdirs();
		}
		
		String imgPath="";
		
		//处理文件上传
		try {
			file.transferTo(new File(dateDir,newFileName));
			imgPath = "\\images"+File.separator+"showImg"+File.separator+dateformat+File.separator+newFileName;
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}

		return imgPath;
	}
}
