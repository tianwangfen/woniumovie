package com.woniuxy.controller;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
/**
 * 订单支付功能
 * @author 余枭春
 *
 */

import com.alipay.api.AlipayApiException;
import com.woniuxy.entity.OrderVO;
import com.woniuxy.service.AlipayService;
import com.woniuxy.service.OrderService;
import com.woniuxy.utils.RedisUtil;
import com.woniuxy.utils.ResponseEntity;

/**
 * 支付
 * 
 * @author 余枭春
 *
 */
@Controller
@RequestMapping("/alipay")
public class AlipayController {
	@Autowired
	private OrderService orderServie;
	@Autowired
	private AlipayService alipayService;
	@Autowired
	private RedisUtil redisUtil;

	/**
	 * 订单支付
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/pagePay/{orderNo}", method = RequestMethod.GET)
	public ResponseEntity<String> pagePay(@PathVariable String orderNo) throws Exception {
		// 从数据库中取订单数据
//		OrderVO order = orderServie.getOrderByOrderNo(orderNo);
		// 从redis中取订单信息
		Object order1 = redisUtil.get("DD" + orderNo);
		if (order1 != null) {
			OrderVO order = (OrderVO) order1;
			BigDecimal totalAmount = order.getTotalMoney();

			String pay = alipayService.pagePay(orderNo, totalAmount);
			return new ResponseEntity<String>(200, "ok", pay);
		} else {
			return new ResponseEntity<String>(500, "订单不存在");
		}
	}

	/**
	 * 付款成功之后返回成功字符串，提醒前台跳转成功页面
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/return")
	public String doReturn(HttpServletRequest req) {
		return "ok";
	}

	/**
	 * 支付后修改订单状态
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping("/notify/{orderNo}")
	@ResponseBody
	public ResponseEntity<String> notify(@PathVariable String orderNo) {
		OrderVO order = orderServie.getOrderByOrderNo(orderNo);
		order.setStatus(OrderController.ORDER_NOTBGINE);// 已付款
		try {
			orderServie.updateOrderStatusByOrderNo(OrderController.ORDER_NOTBGINE, orderNo);
			return new ResponseEntity<String>("状态修改成功");
		} catch (Exception e) {
			return new ResponseEntity<String>(500, "修改订单状态失败");
		}
	}

	/**
	 * 交易关闭
	 * 
	 * @throws AlipayApiException
	 */
	@GetMapping("alipayclose/{orderNo}")
	public ResponseEntity<String> alipaycolse(@PathVariable String orderNo) throws AlipayApiException {
		String close = alipayService.close(orderNo);
		return new ResponseEntity<String>(close);
	}

}
