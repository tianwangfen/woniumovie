package com.woniuxy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.woniuxy.entity.P_function;
import com.woniuxy.service.P_functionService;

@Controller
@RequestMapping("/p_function")
public class P_functionController {
	@Autowired
	P_functionService p_functionService;
	
	@RequestMapping("/all")
	@ResponseBody
	public List<P_function> selectAll(){
		return p_functionService.selectAll();
		
	}
	@RequestMapping("/delete")
	@ResponseBody
	public String deleteFunction(int p_functionId) {
		return p_functionService.deleteFunctionById(p_functionId);
		
	}
}
