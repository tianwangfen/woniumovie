package com.woniuxy.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.woniuxy.entity.Movie;
import com.woniuxy.service.MovieService;
import com.woniuxy.utils.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * @author: crowned
 * @Date: 2020/6/9 23:41
 * @Description: 影片业务控制层
 */
@Controller
@RequestMapping("/movie")
public class MovieController {
    @Autowired
    private MovieService movieService;

    @RequestMapping("/list")
    @ResponseBody
    public PageInfo<Movie> listMovie(Movie movie) {
        PageHelper.startPage(movie.getPageNum(), movie.getPageSize());
        List<Movie> movies = movieService.listMovie();
        PageInfo<Movie> moviePageInfo = new PageInfo<>(movies);
        return moviePageInfo;
    }

    @RequestMapping("/listAll")
    @ResponseBody
    public List<Movie> listMovie() {
        return movieService.listMovie();
    }

    @RequestMapping("/getMovieById/{movieId}")
    @ResponseBody
    public Movie getMovieByMovieId(@PathVariable("movieId") int movieId) {
//        System.out.println(movieId);
        return movieService.getMovieByMovieId(movieId);
    }

    @RequestMapping("/updateMovie")
    @ResponseBody
    public ResponseEntity<Integer> updateMovieById(Movie movie) {
        try {
            if (movieService.updateMovieById(movie) > 0) {
                return new ResponseEntity<Integer>(200, "修改成功！");
            }
        } catch (Exception e) {
            return new ResponseEntity<Integer>(500, "修改失败，请稍后再试...");
        }
        return new ResponseEntity<Integer>(500, "修改失败，请稍后再试...");
    }

    @RequestMapping("/outOfStock")
    @ResponseBody
    public ResponseEntity<Integer> outOfStockById(int movieId) {
        System.out.println(movieId);
        if (movieId != 0) {
            if (movieService.outOfStockById(movieId) > 0) {
                return new ResponseEntity<Integer>(200, "下架成功！");
            }
        }
        return new ResponseEntity<Integer>(500, "服务器正忙，请稍后再试...");
    }

    @RequestMapping("/addMovie")
    @ResponseBody
    public ResponseEntity<Integer> addMovie(Movie movie) {
        System.out.println(movie.getMovieName());
        String movieNo = UUID.randomUUID().toString();
        movie.setMovieNo(movieNo);
        System.out.println(movie.getLength());
        try {
            if (movieService.insertMovie(movie) > 0) {
                System.out.println("成功");
            }
        } catch (Exception e) {
            return new ResponseEntity<Integer>(500, "服务器正忙，请稍后再试...");
        }
        return new ResponseEntity<Integer>(200, "添加成功！");
    }


    @RequestMapping("/upload")
    @ResponseBody
    public ResponseEntity<String> movieUpload(MultipartFile file, HttpServletRequest request) {
        //获取文件名
        String uploadFileName = file.getOriginalFilename();
        try {

            uploadFileName = UUID.randomUUID().toString() + uploadFileName.substring(uploadFileName.indexOf("."));

            //当前项目根路径
            String path = request.getServletContext().getRealPath("");
            System.out.println(path);
            File root = new File(path);

//        File webapps = root.getParentFile(); //得到webapps
//        System.out.println(webapps);
            //得到存放文件的目录
            File fileDir = new File(root, "images/movieImage");
            if (!fileDir.exists()) {
                fileDir.mkdirs();//不存在就创建
            }
            //得到文件存储的路径
            uploadFileName = fileDir.getAbsolutePath() + File.separator + uploadFileName;

            File destFile = new File(uploadFileName);
            file.transferTo(destFile); //写入磁盘
            System.out.println(uploadFileName);


        } catch (Exception e) {
            return new ResponseEntity<String>(500, "服务器正忙，请稍后再试...");
        }

        return new ResponseEntity<String>(200, uploadFileName);

    }
}
