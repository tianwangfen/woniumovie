package com.woniuxy.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.woniuxy.dao.UserDao;
import com.woniuxy.entity.Already_invited;
import com.woniuxy.entity.UserPO;
import com.woniuxy.service.Already_invitedService;
import com.woniuxy.service.UserService;
import com.woniuxy.utils.CodeUtils;
import com.woniuxy.utils.EmailUtils;
import com.woniuxy.utils.RedisUtil;
import com.woniuxy.utils.ResponseEntity;

/**
 * 向被邀请人发送邮件
 * 
 * @author 阳佳
 *
 */
@Controller
@RequestMapping("/email")
public class EmailController {
	@Autowired
	private RedisUtil redisUtil;
	@Autowired
	private Already_invitedService already_invitedService;
	@Autowired
	private UserService userService;

	@RequestMapping("/send")
	@ResponseBody
	public ResponseEntity<String> send(String email) {
		System.out.println("test" + email);
		// 判断email是否已注册
		if (userService.findUserPOByEmail(email) != null) {
			return new ResponseEntity<String>(111, "该邮箱已注册！");
		}
		if (already_invitedService.findByEmail(email) != null) {
			return new ResponseEntity<String>(112, "已邀请该用户！");
		}
		int code = CodeUtils.getCode();
		//将code存在redis中
		redisUtil.set(email, code);
		EmailUtils.sendMail(email, code);
		// 在邀请表中新增一条记录
		Subject currentUser = SecurityUtils.getSubject();
		// 得到session
		Session session = currentUser.getSession();
		// 得到uid
		UserPO userPO = (UserPO) session.getAttribute("user");
		int uid = userPO.getUid();
		already_invitedService.insert(new Already_invited().setEmail(email).setUid(uid));
		return new ResponseEntity<String>(113, "邀请成功");
	}
}
