package com.woniuxy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.woniuxy.entity.Seat;
import com.woniuxy.service.SeatService;
import com.woniuxy.utils.ResponseEntity;

/**
 * 座位业务类，一般的正删改查功能，会有选座的功能
 * 
 * @author 余枭春
 *
 */
@Controller
@RequestMapping("/seat")
public class SeatController {
	@Autowired
	private SeatService seatService;
	
	/**
	 * 通过排片id查找该厅的座位信息
	 * @param hallId
	 * @return
	 */
	@RequestMapping(value = "/list/{scheduleId}",method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<Seat>> listSeatsInfoOfHallByHallId(@PathVariable Integer scheduleId) {
		try {
			List<Seat> result = seatService.listSeatsInfoOfHallByScheduleIdId(scheduleId);
			return new ResponseEntity<List<Seat>>(200, "ok", result);
		} catch (Exception e) {
			return new ResponseEntity<List<Seat>>(500, "找到不到已定的座位");
		}
	}


	//存在redis中的key：影院id-放映点id-厅id-排片id
	//存在redis中的value：list<seat> [行列，价格，状态（订票/空位/不可用）]

}
