package com.woniuxy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.woniuxy.entity.Manager;
import com.woniuxy.service.ManagerService;
import com.woniuxy.utils.ResponseEntity;

@Controller
@RequestMapping("/manager")
public class ManagerController {
	@Autowired
	ManagerService managerService;
	
	
	@RequestMapping("/login")
	public ResponseEntity<Manager> checkAccount(String managerAccount, String managerPwd) {
		
		String account = managerService.selectManagerByAccount(managerAccount);
		if (account!=null) {
			checkPwd(managerAccount, managerPwd);
		}
		return new ResponseEntity<Manager>(200, "账号不存在");
		//重新登录
	}


	private ResponseEntity<Manager> checkPwd(String managerAccount,String managerPwd) {

			String pwd = managerService.selctpwdByAccount(managerAccount);
			if(pwd!=null && pwd.equals(managerPwd)) {
				//进入首页
			}
			return new ResponseEntity<Manager>(500, "账号或密码有误", null);
	}
}
