package com.woniuxy.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.woniuxy.entity.OrderDTO;
import com.woniuxy.entity.OrderPO;
import com.woniuxy.entity.OrderVO;
import com.woniuxy.entity.UserPO;
import com.woniuxy.service.OrderService;
import com.woniuxy.utils.OrderUtils;
import com.woniuxy.utils.ResponseEntity;

/**
 * 订单业务类提供订单生成，订单失效，订单查询的功能
 * 
 * @author 余枭春
 *
 */
@Controller
@RequestMapping("/order")
public class OrderController {
	// 1未付款，2已付款未开场，4待评价，3已退款，5订单失效
	public static final String ORDER_NOTPAYED = "1";
	public static final String ORDER_NOTBGINE = "2";
	public static final String ORDER_HAVERETURNED = "3";
	public static final String ORDER_HAVEWATCHED = "4";
	public static final String ORDER_FAIL = "5";

	@Autowired
	private OrderService orderService;

	/**
	 * 通过用户id查询他所有的订单
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<OrderPO>> listOrderByUserId(Integer userId) {
		try {
			return new ResponseEntity<>(orderService.listOrderByUserId(userId));
		} catch (Exception e) {
			return new ResponseEntity<>(500, "系统正在休息，请稍后访问");
		}
	}

	/**
	 * 前台将订单信息提交
	 * 
	 * @param order
	 * @return
	 */
	@RequestMapping(value = "/new", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<OrderVO> insertOrder(OrderDTO order) {
		Subject currentUser = SecurityUtils.getSubject();
		Session session = currentUser.getSession();
		UserPO user = (UserPO) session.getAttribute("user");
		if (user != null) {
			try {
				order.setOrderNo(OrderUtils.createOrderNo());
				order.setOrdertime(OrderUtils.getTime());
				order.setUserId(user.getUid());
				order.setTelphone(user.getPhoneNum());
				order.setStatus(ORDER_NOTPAYED);
				int res = orderService.insertOrder(order);
				if (res > 0) {
					OrderVO resOrder = orderService.getOrderByOrderNo(order.getOrderNo());
					resOrder.setUser(user);
					return new ResponseEntity<OrderVO>(resOrder);
				} else {
					return new ResponseEntity<OrderVO>(500, "系统正在休息呢，大人稍后再来玩呀~");
				}
			} catch (Exception e) {
				return new ResponseEntity<OrderVO>(500, "系统正在休息呢，大人稍后再来玩呀~");
			}
		} else {
			return new ResponseEntity<OrderVO>(500, "您没得登陆哦，请先登录再下单好不好呀~");
		}
	}
	
	

}
