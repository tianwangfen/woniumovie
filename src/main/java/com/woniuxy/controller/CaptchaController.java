package com.woniuxy.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wf.captcha.GifCaptcha;
import com.wf.captcha.utils.CaptchaUtil;
import com.woniuxy.utils.RedisUtil;
import com.woniuxy.utils.ResponseEntity;

/**
 * 功能：生成图形验证码
 * 
 * @author 阳佳
 *
 */
@Controller
public class CaptchaController {
	@Autowired
	private RedisUtil redisUtil;

	@RequestMapping("/captcha")
	public void captcha(HttpServletRequest request, HttpServletResponse response) throws Exception {

		GifCaptcha gifCaptcha = new GifCaptcha(130, 48, 4);
		CaptchaUtil.out(gifCaptcha, request, response);
		String verCode = gifCaptcha.text();
		CaptchaUtil.out(gifCaptcha, request, response);
		redisUtil.set("captcha", verCode, 120);
		System.out.println("这是contrller产生的验证码" + verCode);
	}
}
