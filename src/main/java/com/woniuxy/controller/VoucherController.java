package com.woniuxy.controller;

import java.util.List;
import java.util.UUID;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.woniuxy.entity.UserPO;
import com.woniuxy.entity.Voucher;
import com.woniuxy.service.Already_invitedService;
import com.woniuxy.service.Record_VoucherService;
import com.woniuxy.service.VoucherService;
import com.woniuxy.utils.RedisUtil;
import com.woniuxy.utils.ResponseEntity;
import com.woniuxy.utils.TimeUtils;
/**
 * 
 * @author 阳佳
 *
 */
@Controller
@RequestMapping("/voucher")
public class VoucherController {
	@Autowired
	private VoucherService voucherService;
	@Autowired
	private RedisUtil redisUtil;
	@Autowired
	private Already_invitedService already_invitedService;
	@Autowired
	private Record_VoucherService record_VoucherService;

	@RequestMapping("/send")
	@ResponseBody
	public ResponseEntity<String> AutoSendVoucher(String email, String code) {
		// 判断是否邀请过该用户
		if (already_invitedService.findByEmail(email) == null) {
			return new ResponseEntity<String>(111, "未邀请过该用户");
		}
		// 判断邀请人是否已经兑换过
		if (record_VoucherService.findByEmail(email) != null) {
			return new ResponseEntity<String>(114, "已兑换过代金券");
		}
		// 从redis中取出验证码
		String res = String.valueOf(redisUtil.get(email));
		// 判断输入的邀请码是否正确
		if (!res.equals(code)) {
			return new ResponseEntity<String>(112, "验证码不正确");
		}
		// 产生唯一标识码
		String num = UUID.randomUUID().toString();
		voucherService.insertVoucher(new Voucher().setNum(num).setMoney(5).setStatus(2).setGetTime(TimeUtils.getTime())
				.setInvalidTime(TimeUtils.addDate(TimeUtils.getTime(), 72)));
		return new ResponseEntity<String>(113, "兑换成功");
	}

	@RequestMapping("/find")
	@ResponseBody
	public ResponseEntity<List<Voucher>> find() {
		// 得到用户uid
		Subject currentUser = SecurityUtils.getSubject();
		Session session = currentUser.getSession();
		UserPO userPO = (UserPO) session.getAttribute("user");
		int uid = userPO.getUid();
		return new ResponseEntity<List<Voucher>>(record_VoucherService.findRecord_VoucherByUid(uid));
		
	}
}
