package com.woniuxy.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.woniuxy.entity.UserPO;
/**
 * 
 * @author 阳佳
 *
 */
@Mapper
public interface UserDao {
	@Insert("insert into user values(default,#{account},#{password},#{phoneNum},#{email},#{gender},#{birthdate},#{address},#{status},#{headImage})")
	public int insertUser(UserPO userPO);
	@Select("select count(*) from user where account=#{account}")
	public int findUserNumByAccount(String account);
	@Select("select *  from user where account=#{account}")
	public UserPO findUserPOByAccount(String account);
	@Select("select *  from user where uid=#{uid}")
	public UserPO findUserPOByUid(int uid);
	@Select("select *  from user where phoneNum=#{phoneNum}")
	public UserPO findUserPOByPhoneNum(String phoneNum);
	@Select("select *  from user where email=#{email}")
	public UserPO findUserPOByEmail(String email);
	@Update("update user set password=#{password} where phoneNum=#{phoneNum}")
	public int resetPassword(String password,String phoneNum);
	@Update("update user set email=#{email},gender=#{gender},birthdate=#{birthdate},address=#{address},headImage=#{headImage} where uid=#{uid}")
	public int updateUserInfoByUid(UserPO userPO,int uid);
}
