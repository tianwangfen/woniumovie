package com.woniuxy.dao;

import java.util.List;

import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.woniuxy.entity.Seat;

@Repository
public interface SeatDao {
	@Select("select * from seat where scheduleId=#{scheduleId}")
	List<Seat> listSeatsInfoOfHallByScheduleIdId(Integer scheduleId);
}
