package com.woniuxy.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import com.woniuxy.entity.OrderDetails;

/**
 * 订单明细dao类，采用注解实现增删查改
 * 
 * @author 余枭春
 *
 */
public interface OrderDetailsDao {
	@Insert("insert into orderDetails(col,row,movieName,orderId,orderNo,price,place) values(#{col},#{row},#{movieName},#{orderId},#{orderNo},#{price},#{place})")
	int insertOrderDetails(OrderDetails orderDetail);

	@Select("select * from orderDetails where orderId=#{orderId}")
	List<OrderDetails> listDetailsByOrderId(Integer orderId);

	@Select("select * from orderDetails where orderNo=#{orderNo}")
	List<OrderDetails> listDetailsByOrderNo(String orderNo);

}
