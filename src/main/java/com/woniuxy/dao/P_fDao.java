package com.woniuxy.dao;
/**
 * 操作功能和数量的类,套餐编辑的类
 * @author Administrator
 *
 */

import java.util.List;

import com.woniuxy.entity.P_fBean;

public interface P_fDao {
	/**
	 * 查询当前当餐下的所有功能及其数量
	 */
	List<P_fBean> allP_fBean(int packageId);
	
	/**
	 * 根据packageId删除p_f表中数据
	 * @return 
	 */
	int del(int packageId);
	}
