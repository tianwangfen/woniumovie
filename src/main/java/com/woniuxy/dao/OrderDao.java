package com.woniuxy.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.woniuxy.entity.OrderDTO;
import com.woniuxy.entity.OrderPO;
import com.woniuxy.entity.OrderVO;

/**
 * 订单dao类，采用注解的方式获取数据库数据
 * 
 * @author 余枭春
 *
 */
@Repository
public interface OrderDao {
	/**
	 * 通过用户id查询所有订单
	 * 
	 * @param userId 用户id
	 * 
	 * @return
	 */
	@Select("select * from order where userId=#{userId}")
	List<OrderPO> listOrderByUserId(Integer userId);

	/**
	 * 新建订单
	 * 
	 * @param order
	 * @return
	 */
	@Insert("insert into order(orderNo,userId,telphone,ordertime,totalMoney,payType,payTime,status) values(#{orderNo},#{userId},#{telphone},#{ordertime},#{totalMoney},#{payType},#{payTime},#{status})")
	int insertOrder(OrderDTO order);

	/**
	 * 通过订单编号获取订单
	 * 
	 * @param orderNo
	 * @return
	 */
	@Select("select * from order where orderNo=#{orderNo}")
	OrderVO getOrderByOrderNo(String orderNo);

	/**
	 * 通过订单id获取订单
	 * 
	 * @param orderId
	 * @return
	 */
	@Select("select * from order where orderId=#{orderId}")
	OrderVO getOrderByOrderId(Integer orderId);

	/**
	 * 更新订单类的所有信息
	 * 
	 * @param order
	 * @return
	 */
	@Update("update order set orderNo=#{orderNo},userId=#{userId},telphone=#{telphone},ordertime=#{ordertime},totalMoney=#{totalMoney},payType=#{payType},payTime=#{payTime},status=#{status} where orderId=#{orderId}")
	int updateOrder(OrderPO order);

	/**
	 * 更新订单状态  通过订单编号 更改订单状态
	 * 
	 * @param status
	 * @param orderNo
	 * @return
	 */
	@Update("update order set status=#{status} where orderNo=#{orderNo}")
	int updateOrderStatusByOrderNo(String status, String orderNo);
}
