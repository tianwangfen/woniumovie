package com.woniuxy.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.woniuxy.entity.Show;
import com.woniuxy.entity.ShowDto;
/**
 * 
 * @author 邹扬
 *
 */
@Mapper
public interface ShowDao {

	/*
	 * 获取所有的放映点
	 */
	List<Show> listAll();
	/*
	 * 条件查询所有
	 */
	List<Show> ListByCondition(ShowDto showDto);
	/*
	 * 插入一个放映点
	 */
	int insertShow(Show show);
	/*
	 * 批量删除
	 * xml中配置
	 */
	int batchDeleteShows(List<Integer> ids);
	
	/*
	 * 更新
	 */
	int updateShow(Show show);
	
	/*
	 * 通过id查
	 */
	Show getShowById(Integer showId);
	/*
	 * 通过影院id查该影院所有放映点
	 */
	List<Show> getShowByCinemaId(Integer cinemaId);
	
}
