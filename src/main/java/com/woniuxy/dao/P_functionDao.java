package com.woniuxy.dao;
/**
 * 功能管理的dao
 * @author Administrator
 *
 */

import java.util.List;

import com.woniuxy.entity.P_function;

public interface P_functionDao {
	/**
	 * 查询所有的功能信息
	 * @return
	 */
	public List<P_function> selectAll();
	
	/**
	 * 删除当前功能
	 */
	public String deleteFunctionById(int p_functionId);
	
	/**
	 * 根据功能id查询 p_f表中是否有数据
	 */
	public String selectP_fIdByid(int p_functionId);
}
