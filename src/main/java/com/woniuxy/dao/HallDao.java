package com.woniuxy.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.woniuxy.entity.Hall;
import com.woniuxy.provider.HallProvider;

/**
 * 厅dao层
 * 
 * @author 余枭春
 *
 */
@Repository
public interface HallDao {
	@Insert("insert into hall(hallName,image,showId,description,cols,rows) values(#{hallName},#{image},#{showId},#{description},#{cols},#{rows})")
	int addHall(Hall hall);

	@Update("update hall set hallName=#{hallName},image=#{image},showId=#{showId},description=#{description},cols=#{cols},rows=#{rows} where hallId=#{hallId}")
	int updateHall(Hall hall);

	@Delete("delete from hall where hallId=#{hallId}")
	int deleteHallByHallId(Integer hallId);

	@Delete("delete from hall where hallId=#{hallId}")
	int batchDeleteHall(Integer[] hallIds);

	/**
	 * 按条件查询,放映点id和展厅名字
	 * 
	 * @param hall
	 * @return
	 */
	@SelectProvider(type = HallProvider.class, method = "listHall")
	List<Hall> listHall(Hall hall);

	@Select("select * from hall where hallId=#{hallId}")
	@Results({ @Result(id = true, column = "hallId", property = "hallId"),
			@Result(column = "showId", property = "show", one = @One(select = "com.woniuxy.dao.ShowDao.getShowById")) })
	Hall getHallByHallId(Integer hallId);

	@Select("select * from hall where hallId=#{hallId}")
	Hall findHallByHallId(Integer hallId);

	@Select("select * from hall where showId=#{showId} and hallName=#{hallName}")
	Hall getHallByShowIdAndName(Integer showId, String hallName);

	@Select("select * from hall where showId=#{showId}")
	List<Hall> getHallsByShowId(Integer showId);
}
