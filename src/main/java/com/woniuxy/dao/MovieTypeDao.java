package com.woniuxy.dao;

import com.woniuxy.entity.MovieType;

public interface MovieTypeDao {
    /**
    * @Description: 根据影片类别id查询具体类别信息
    * @Param:  movieTypeId
    * @return:  MovieType
    * @Author: crowned
    * @Date: 2020/6/12
    */
    MovieType getBymovieTypeId(int movieTypeId);
}
