package com.woniuxy.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.woniuxy.entity.P_fBean;
import com.woniuxy.entity.Packages;
@Repository
public interface PackagesDao {
	/**
	 * 展示所有的套餐信息
	 */
	List<Packages> listAll();
	
	/**
	 * 删除当前套餐
	 * @param packageId
	 * @return
	 */
	int del(int packageId);
	
	
	
	
	
}
