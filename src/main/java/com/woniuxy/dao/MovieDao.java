package com.woniuxy.dao;

import com.woniuxy.entity.Movie;
import com.woniuxy.utils.ResponseEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author: crowned
 * @Date: 2020/6/9 23:39
 * @Description: 影片数据库操作类
 */
@Repository
public interface MovieDao {
    /**
     * @Description: 查询所有影片
     * @Param:
     * @return: movie
     * @Author: crowned
     * @Date: 2020/6/11
     */
    List<Movie> listMovie();


    /**
     * @Description: 根据电影编号查询单个影片
     * @Param: movieNo
     * @return: movie
     * @Author: crowned
     * @Date: 2020/6/11
     */
    Movie getMovieByMovieNo(int movieNo);

    /**
    * @Description: 根据电影id查询单个影片
    * @Param:  movieId
    * @return:  movie
    * @Author: crowned
    * @Date: 2020/6/12
    */
    Movie getMovieByMovieId(int movieId);
    /**
     * @Description: 添加影片
     * @Param: movie
     * @return: int
     * @Author: crowned
     * @Date: 2020/6/11
     */
    int insertMovie(Movie movie);

    /**
     * @Description: 修改电影信息
     * @Param: 影片id
     * @return: int
     * @Author: crowned
     * @Date: 2020/6/11
     */
    int updateMovieById(Movie movie);

    /**
     * @Description: 删除影片信息
     * @Param: movieId
     * @return: int
     * @Author: crowned
     * @Date: 2020/6/11
     */
    int deleteMovieById(int movieId);


    /**
    * @Description: 下架影片
    * @Param:  movieId
    * @return:
    * @Author: crowned
    * @Date: 2020/6/23
    */
    int outOfStockById(int movieId);
}
