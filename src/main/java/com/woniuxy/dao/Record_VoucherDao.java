package com.woniuxy.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.woniuxy.entity.Record_Voucher;
import com.woniuxy.entity.Voucher;
/**
 * 
 * @author 阳佳
 *
 */
@Mapper
public interface Record_VoucherDao {
	@Insert("insert into record_voucher values(default,#{uid},#{vid},#{getTime},#{useTime},#{status},#{email})")
	public int insertRecord(Record_Voucher record_Voucher);

	@Select("select * from record_voucher where email=#{email}")
	public Record_Voucher findByEmail(String email);

	@Update("update record_voucher set status=0 where vid=#{vid}")
	public int updateStatus(int vid);

	@Select("select * from record_voucher rv,voucher v where rv.uid=#{uid} and rv.vid=v.vid")
	public List<Voucher> findRecord_VoucherByUid(int uid);
}
