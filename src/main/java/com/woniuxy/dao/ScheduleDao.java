package com.woniuxy.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import com.woniuxy.entity.Schedule;
import com.woniuxy.provider.ScheduleProvider;

/**
 * 排片dao层
 * 
 * @author 余枭春
 *
 */
@Repository
public interface ScheduleDao {
	/**
	 * 新增一个排片信息，所有信息封装在schedule中
	 * 
	 * @param schedule
	 * @return 成功返回1，失败返回0
	 */
	@Insert("insert into schedule(showId,movieId,hallId,beginDate,beginTime,endTime,discount,movieSessionId) values(#{showId},#{movieId},#{hallId},#{beginDate},#{beginTime},#{endTime},#{discount},#{movieSessionId})")
	Integer insertSchedule(Schedule schedule);

	/**
	 * 更新拍片信息，schedule中封装所有的信息
	 * 
	 * @param schedule
	 * @return
	 */
	@Update("update schedule set showId=#{showId},movieId=#{movieId},hallId=#{hallId},beginDate=#{beginDate},beginTime=#{beginTime},endTime=#{endTime},discount=#{discount},movieSessionId=#{movieSessionId} where scheduleId=#{scheduleId}")
	Integer updateSchedule(Schedule schedule);

	/**
	 * 按照条件查询符合条件的说有排片信息
	 * 
	 * @param schedule
	 * @return
	 */
	@SelectProvider(type = ScheduleProvider.class, method = "listSchedule")
	@Results({ @Result(id = true, column = "scheduleId", property = "scheduleId"),
			@Result(column = "showId", property = "show", one = @One(select = "com.woniuxy.dao.ShowDao.getShowById")),
			@Result(column = "movieId", property = "movie", one = @One(select = "com.woniuxy.dao.MovieDao.getMovieByMovieId")),
			@Result(column = "hallId", property = "hall", one = @One(select = "com.woniuxy.dao.HallDao.getHallByHallId"))
	})
	List<Schedule> listSchedule(Schedule schedule);

	/**
	 * 查询当天最近的排片结束时间
	 * 
	 * @param beginDate
	 * @return
	 */
	@Select("select max(endTime) endTime from schedule where beginDate=#{beginDate} and hallId=#{hallId}")
	String getLastEndTimeWithBeginDate(String beginDate, Integer hallId);

	@Select("select max(endTime) endTime from schedule where beginDate=#{beginDate} and hallId=#{hallId} and beginTime<#{beginTime}")
	String getExLastEndTimeWithBeginDate(String beginDate, Integer hallId, String beginTime);

	/**
	 * 通过id获取一个排片信息，并且封装电影，影厅，放映点的信息
	 * 
	 * @param scheduleId
	 * @return
	 */
	@Select("select * from schedule where scheduleId=#{scheduleId}")
	@Results({ @Result(id = true, column = "scheduleId", property = "scheduleId"),
			@Result(column = "showId", property = "show", one = @One(select = "com.woniuxy.dao.ShowDao.getShowById")),
			@Result(column = "movieId", property = "movie", one = @One(select = "com.woniuxy.dao.MovieDao.getMovieByMovieId")),
			@Result(column = "hallId", property = "hall", one = @One(select = "com.woniuxy.dao.HallDao.getHallByHallId"))
			// TODO 通过厅id获取展厅信息
	})
	Schedule getScheduleByScheduleId(Integer scheduleId);

	/**
	 * 根据排片id删除排片信息
	 * 
	 * @param schedule
	 * @return
	 */
	@Delete("delete from schedule where scheduleId=#{scheduleId}")
	Integer deleteSchedule(Integer scheduleId);

	/**
	 * 根据电影的id查询已经排的最新场次
	 * 
	 * @param movieId
	 * @return 已经排好的最新的场次
	 */
	@Select("select max(movieSessionId) movieSessionId from schedule where movieId=#{movieId}")
	Integer getMaxMovieSessionIdByMovieId(Integer movieId);

}
