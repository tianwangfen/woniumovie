package com.woniuxy.dao;

import com.woniuxy.entity.Actor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @Description: 演员表Dao
* @Param:
* @return:
* @Author: crowned
* @Date: 2020/6/12
*/
@Repository
public interface ActorDao {
    Actor getActorById(int actorId);
}
