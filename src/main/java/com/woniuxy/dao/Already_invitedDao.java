package com.woniuxy.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.woniuxy.entity.Already_invited;
/**
 * 
 * @author 阳佳
 *
 */
@Mapper
public interface Already_invitedDao {
	@Insert("insert into already_invited values(default,#{uid},#{email})")
	public int insert(Already_invited already_invited);

	@Select("select * from already_invited where email=#{email}")
	public Already_invited findByEmail(String email);

	@Select("select email from already_invited where uid=#{uid}")
	public String findEmailByUid(int uid);
}
