package com.woniuxy.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.woniuxy.entity.Voucher;

/**
 * 优惠券相关操作
 * 
 * @author 阳佳
 *
 */
@Mapper
public interface VoucherDao {
	@Insert("insert into voucher values(default,#{num},#{status},#{money},#{getTime},#{invalidTime})")
	public int insertVoucher(Voucher voucher);// 系统生成的优惠券的状态都为未使用(2) 金额都为(5元) 金额可更改 由平台确定

	@Select("select * from voucher where num=#{num}")
	public Voucher findVoucherByNum(String num);

	@Update("update voucher set status=0 where vid=#{vid}")
	public int updateStatus(int vid);

	@Select("select status from voucher where vid=#{vid}")
	public int getCurrentStatus(int vid);
}
