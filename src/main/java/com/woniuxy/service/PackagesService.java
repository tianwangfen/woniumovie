package com.woniuxy.service;

import java.util.List;

import com.woniuxy.entity.Packages;

public interface PackagesService {
	public List<Packages> listAll();

	public int del(int packageId);
}
