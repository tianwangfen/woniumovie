package com.woniuxy.service;

import com.woniuxy.entity.Already_invited;
/**
 * 
 * @author 阳佳
 *
 */
public interface Already_invitedService {
	public int insert(Already_invited already_invited);

	public Already_invited findByEmail(String email);

	public String findEmailByUid(int uid);
}
