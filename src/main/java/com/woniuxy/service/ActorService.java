package com.woniuxy.service;

import com.woniuxy.entity.Actor;

public interface ActorService {
    Actor getActorById(int actorId);
}
