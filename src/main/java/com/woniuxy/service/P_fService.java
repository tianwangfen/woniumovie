package com.woniuxy.service;

import java.util.List;

import com.woniuxy.entity.P_fBean;

public interface P_fService {
	/**
	 * 查询当前当餐下的所有功能及其数量
	 */
	List<P_fBean> allP_fBean(int packageId);
	
	/**
	 * 删除当前套餐相关的p_f数据
	 * @param packageId
	 * @return
	 */
	int del(int packageId);

}
