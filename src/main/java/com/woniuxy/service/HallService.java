package com.woniuxy.service;

import java.util.List;

import com.woniuxy.entity.Hall;
/**
 * 
 * @author 余枭春
 *
 */
public interface HallService {
	int insertHall(Hall hall);

	int updateHall(Hall hall);

	int deleteHallByHallId(Integer hallId);

	List<Hall> listHall(Hall hall);

	Hall getHallByHallId(Integer hallId);

	Hall getHallByShowIdAndName(Integer showId, String hallName);

	List<Hall> getHallsByShowId(Integer showId);
}
