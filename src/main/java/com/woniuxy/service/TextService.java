package com.woniuxy.service;

/**
 * 发送验证码
 * 
 * @author 阳佳
 *
 */
public interface TextService {
	/**
	 * 
	 * @param to 发送给谁
	 * @param templateId 发送模板    免费开发模板id为1
	 * @param data 发送的内容
	 * @throws Exception 
	 */
	public void send(String to, String templateId, String[] data) throws Exception;
	/**
	 * 短信验证  判断验证码是否一致
	 * @param phoneNum 手机号
	 * @param code 验证码
	 * @return
	 */
	public boolean validateText(String phoneNum,String code);
}
