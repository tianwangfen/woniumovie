package com.woniuxy.service;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.alipay.api.AlipayApiException;
/**
 * 支付服务类
 * @author 余枭春
 *
 */

public interface AlipayService {
	 /**
     * 订单支付
     * @param orderNo    订单编号（唯一）
     * @param totalAmount   订单价格
     * @param subject       商品名称
     */
    String pagePay(String orderNo,BigDecimal totalAmount) throws Exception;
    /**
     * 退款
     * @param orderNo    订单编号
     * @param refundReason  退款原因
     * @param refundAmount  退款金额
     * @param outRequestNo  标识一次退款请求，同一笔交易多次退款需要保证唯一，如需部分退款，则此参数必传
     */
    String refund(String outTradeNo,String refundReason,Integer refundAmount,String outRequestNo) throws AlipayApiException;
    /**
     * 交易关闭
     * @param orderNo订单编号（唯一）
     */
    String close(String orderNo) throws AlipayApiException;
    
    /**
     * 退款查询
     * @param orderNo 订单编号（唯一）
     * @param outRequestNo 标识一次退款请求，同一笔交易多次退款需要保证唯一，如需部分退款，则此参数必传
     */
    String refundQuery(String orderNo,String outRequestNo) throws AlipayApiException;
}
