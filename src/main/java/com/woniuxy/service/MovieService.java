package com.woniuxy.service;


import com.woniuxy.entity.Movie;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: crowned
 * @Date: 2020/6/9 23:40
 * @Description: 影片业务逻辑类
 */

public interface MovieService {
    List<Movie> listMovie();

    Movie getMovieByMovieNo(int movieNo);


    int insertMovie(Movie movie);

    int updateMovieById(Movie movie);

    int deleteMovieById(int movieId);

    Movie getMovieByMovieId(int movieId);

    int outOfStockById(int movieId);

}
