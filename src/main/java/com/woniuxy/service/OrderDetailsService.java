package com.woniuxy.service;

import java.util.List;

import com.woniuxy.entity.OrderDTO;
import com.woniuxy.entity.OrderDetails;

/**
 * 
 * @author 余枭春
 *
 */
public interface OrderDetailsService {
	List<OrderDetails> listDetailsByOrderId(Integer orderId);

	List<OrderDetails> listDetailsByOrderNo(String orderNo);

	int insertOrderDetails(OrderDetails orderDetails);

	int insertOrderDetailsWithOrder(OrderDTO order);
}
