package com.woniuxy.service;

import com.woniuxy.entity.Voucher;
/**
 * 
 * @author 阳佳
 *
 */
public interface VoucherService {
	public int insertVoucher(Voucher voucher);

	public Voucher findVoucherByNum(String num);

	public int getCurrentStatus(int vid);

	public int updateStatus(int vid);
}
