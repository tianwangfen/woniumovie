package com.woniuxy.service;

import java.util.List;

import com.woniuxy.entity.Seat;

public interface SeatService {
	List<Seat> listSeatsInfoOfHallByScheduleIdId(Integer scheduleId);
}
