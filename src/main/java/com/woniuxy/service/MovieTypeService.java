package com.woniuxy.service;

import com.woniuxy.entity.MovieType;

public interface MovieTypeService {
    MovieType getBymovieTypeId(int movieTypeId);
}
