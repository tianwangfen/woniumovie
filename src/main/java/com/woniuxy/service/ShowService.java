package com.woniuxy.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.PageInfo;
import com.woniuxy.entity.Show;
import com.woniuxy.entity.ShowDto;
import com.woniuxy.entity.ShowVo;
import com.woniuxy.utils.ResponseEntity;
/**
 * 
 * @author 邹扬
 *
 */
public interface ShowService {
	/*
	 * 获取所有的放映点
	 */
	public ShowVo listAll();

	/*
	 * 根据条件查询
	 */
	public ShowVo ListByCondition(ShowDto showDto);

	/*
	 * 插入一个放映点
	 */
	public int insertShow(Show show);

	/*
	 * 批量删除 xml中配置
	 */
	public int batchDeleteShows(List<Integer> ids);

	/*
	 * 更新
	 */
	public int updateShow(Show show);

	/*
	 * 通过id查
	 */
	public Show getShowById(Integer id);
	/*
	 * 获取所有放映点
	 */
	public List<Show> listALL();
	/*
	 * 通过影院id查该影院所有放映点
	 */
	public List<Show> getShowByCinemaId(Integer cid);
	
}
