package com.woniuxy.service;

import com.woniuxy.entity.UserPO;
/**
 * 
 * @author 阳佳
 *
 */
public interface UserService {
	public int insertUser(UserPO userPO);
	public UserPO findUserPOByAccount(String account);
	public UserPO findUserPOByPhoneNum(String phoneNum);
	public UserPO findUserPOByEmail(String email);
	public int resetPassword(String password,String phoneNum);
	public int updateUserInfoByUid(UserPO userPO,int uid);
	public UserPO findUserPOByUid(int uid);
}
