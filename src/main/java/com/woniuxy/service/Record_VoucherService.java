package com.woniuxy.service;

import java.util.List;

import com.woniuxy.entity.Record_Voucher;
import com.woniuxy.entity.Voucher;
/**
 * 
 * @author 阳佳
 *
 */
public interface Record_VoucherService {
	public int insertRecord(Record_Voucher record_Voucher);
	public Record_Voucher findByEmail(String email);
	public List<Voucher> findRecord_VoucherByUid(int uid);
}
