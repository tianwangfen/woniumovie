package com.woniuxy.service;

import java.util.List;

import com.woniuxy.entity.Cinema;

public interface CinemaService {
	/*
	 * 基本信息查看
	 */
	List<Cinema> listAll();
	/*
	 * 根据packageId查询影院
	 */
	Cinema selectCinemaBypackageId(int packageId);
}
