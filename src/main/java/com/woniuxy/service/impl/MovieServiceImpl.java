package com.woniuxy.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.woniuxy.dao.MovieDao;
import com.woniuxy.entity.Movie;
import com.woniuxy.service.MovieService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;

/**
 * @author: crowned
 * @Date: 2020/6/9 23:40
 * @Description: 影片业务逻辑实现类
 */
@Service
@Data
public class MovieServiceImpl implements MovieService {
    @Autowired
    private MovieDao movieDao;

    @Override
    public List<Movie> listMovie() {

        //获得所有影片信息
        List<Movie> movies = movieDao.listMovie();
        //判断影片状态
        Iterator<Movie> movieIterator = movies.iterator();
        while (movieIterator.hasNext()) {
            if (movieIterator.next().getMovieStatus() > 1) {
                //影片状态码等于2 或大于1 皆为下架状态，从显示list中移除
                movieIterator.remove();
            }
        }
        //获得不包含下架状态的所有影片
        return movies;
    }

    @Override
    public Movie getMovieByMovieNo(int movieNo) {
        return movieDao.getMovieByMovieNo(movieNo);
    }


    @Override
    public int insertMovie(Movie movie) {
        return movieDao.insertMovie(movie);
    }

    @Override
    public int updateMovieById(Movie movie) {
        return movieDao.updateMovieById(movie);
    }

    @Override
    public int deleteMovieById(int movieId) {
        return movieDao.deleteMovieById(movieId);
    }

    @Override
    public Movie getMovieByMovieId(int movieId) {
        return movieDao.getMovieByMovieId(movieId);
    }

    @Override
    public int outOfStockById(int movieId) {
        return movieDao.outOfStockById(movieId);
    }


}
