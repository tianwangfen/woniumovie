package com.woniuxy.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.woniuxy.dao.CinemaDao;
import com.woniuxy.entity.Cinema;
import com.woniuxy.service.CinemaService;

@Service
public class CinemaServiceImpl implements CinemaService{
	
	@Autowired
	private CinemaDao cinemaDao;

	@Override
	public List<Cinema> listAll() {
		return cinemaDao.listAll();
	
	}


	@Override
	public Cinema selectCinemaBypackageId(int packageId) {
		// TODO Auto-generated method stub
		return cinemaDao.selectCinemaBypackageId(packageId);
	}

}
