package com.woniuxy.service.impl;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.woniuxy.dao.VoucherDao;
import com.woniuxy.entity.Record_Voucher;
import com.woniuxy.entity.UserPO;
import com.woniuxy.entity.Voucher;
import com.woniuxy.service.Already_invitedService;
import com.woniuxy.service.Record_VoucherService;
import com.woniuxy.service.VoucherService;
import com.woniuxy.utils.RedisUtil;
import com.woniuxy.utils.TimeUtils;

import ch.qos.logback.core.util.TimeUtil;
/**
 * 
 * @author 阳佳
 *
 */
@Service
public class VoucherServiceImpl implements VoucherService {
	@Autowired
	private VoucherDao voucherDao;
	@Autowired
	private Record_VoucherService record_VoucherService;
	@Autowired
	private Already_invitedService already_invitedService;
	private static final String REDIS_PREFIX = "vid:";
	@Autowired
	private RedisUtil redisUtil;

	@Override
	@Transactional
	public int insertVoucher(Voucher voucher) {
		// 系统生成代金券
		int res = voucherDao.insertVoucher(voucher);
		// 与当前用户表示绑定
		// 从session中取到用户id
		Subject currentUser = SecurityUtils.getSubject();
		Session session = currentUser.getSession();
		UserPO userPO = (UserPO) session.getAttribute("user");
		int uid = userPO.getUid();
		// 得到代金券vid
		Voucher result = voucherDao.findVoucherByNum(voucher.getNum());
		int vid = result.getVid();
		// 得到被邀请人的email
		String email = already_invitedService.findEmailByUid(uid);
		// 生成一条Record_Voucher记录
		Record_Voucher record_Voucher = new Record_Voucher().setUid(uid).setVid(vid).setGetTime(TimeUtils.getTime())
				.setStatus(2).setEmail(email);
		record_VoucherService.insertRecord(record_Voucher);
		// 将代金券vid存入redis中 time为秒
		redisUtil.set(REDIS_PREFIX + vid, vid, 10);
		return res;
	}

	@Override
	public Voucher findVoucherByNum(String num) {
		return voucherDao.findVoucherByNum(num);
	}

	@Override
	public int getCurrentStatus(int vid) {
		return voucherDao.getCurrentStatus(vid);
	}

	@Override
	public int updateStatus(int vid) {
		return voucherDao.updateStatus(vid);
	}

}
