package com.woniuxy.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.woniuxy.dao.P_functionDao;
import com.woniuxy.entity.P_function;
import com.woniuxy.service.P_functionService;
@Service
public class P_functionServiceImpl implements P_functionService{
	@Autowired
	P_functionDao p_functionDao ;
	@Override
	public List<P_function> selectAll() {
		// TODO Auto-generated method stub
		return p_functionDao.selectAll();
	}
	/**
	 * 根据功能id删除当前功能
	 */
	@Override
	public String deleteFunctionById(int p_functionId) {
		//判断当前id是否在套餐内
		
		return p_functionDao.deleteFunctionById(p_functionId);
	}

}
