package com.woniuxy.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alipay.api.AlipayApiException;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradeCloseRequest;
import com.alipay.api.request.AlipayTradeFastpayRefundQueryRequest;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.woniuxy.configuration.AlipayConfiguration;
import com.woniuxy.entity.OrderDetails;
import com.woniuxy.service.AlipayService;
import com.woniuxy.service.OrderDetailsService;

/**
 * 支付服务实现类
 * 
 * @author 余枭春
 *
 */
@Service
public class AlipayServiceImpl implements AlipayService {

	DefaultAlipayClient alipayClient = new DefaultAlipayClient(AlipayConfiguration.GATEWAYURL,
			AlipayConfiguration.APP_ID, AlipayConfiguration.MERCHANT_PRIVATE_KEY, "json", AlipayConfiguration.CHARSET,
			AlipayConfiguration.ALIPAY_PUBLIC_KEY, AlipayConfiguration.SIGN_TYPE);
	@Autowired
	private OrderDetailsService orderDetailsService;

	/**
	 * 退款
	 */
	@Override
	public String refund(String orderNo, String refundReason, Integer refundAmount, String outRequestNo)
			throws AlipayApiException {
		AlipayTradeRefundRequest alipayRequest = new AlipayTradeRefundRequest();

		/** 调取接口 */
		alipayRequest.setBizContent("{\"out_trade_no\":\"" + orderNo + "\"," + "\"refund_amount\":\"" + refundAmount
				+ "\"," + "\"refund_reason\":\"" + refundReason + "\"," + "\"out_request_no\":\"" + outRequestNo
				+ "\"}");
		String result = alipayClient.execute(alipayRequest).getBody();
		return result;
	}

	/**
	 * 订单关闭
	 */
	@Override
	public String close(String orderNo) throws AlipayApiException {
		AlipayTradeCloseRequest alipayRequest = new AlipayTradeCloseRequest();

		alipayRequest.setBizContent("{\"out_trade_no\":\"" + orderNo + "\"," + "\"trade_no\":\"" + "" + "\"}");

		String result = alipayClient.execute(alipayRequest).getBody();

		return result;
	}

	/**
	 * 查询退款
	 */
	@Override
	public String refundQuery(String orderNo, String outRequestNo) throws AlipayApiException {
		AlipayTradeFastpayRefundQueryRequest alipayRequest = new AlipayTradeFastpayRefundQueryRequest();

		/** 请求接口 */
		alipayRequest.setBizContent(
				"{\"out_trade_no\":\"" + orderNo + "\"," + "\"out_request_no\":\"" + outRequestNo + "\"}");

		/** 格式转换 */
		String result = alipayClient.execute(alipayRequest).getBody();

		return result;
	}

	/**
	 * 付款
	 */
	@Override
	public String pagePay(String orderNo, BigDecimal totalAmount) throws Exception {
		AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
		/** 同步通知，支付完成后，支付成功页面 */
		alipayRequest.setReturnUrl(AlipayConfiguration.return_url);
		/** 异步通知，支付完成后，需要进行的异步处理 */
		alipayRequest.setNotifyUrl(AlipayConfiguration.notify_url+"/"+orderNo);
		String subject = "您正在完成与蜗牛电影的订单";
		String body = "";
		List<OrderDetails> details = orderDetailsService.listDetailsByOrderNo(orderNo);
		body += "观影点:" + details.get(0).getPlace() + "\\n电影:" + details.get(0).getMovieName() + "\\n";
		for (int i = 0; i < details.size(); i++) {
			OrderDetails detail = details.get(i);
			body += "座位号:" + detail.getRow() + "-" + detail.getCol() + "\t" + "单价:" + detail.getPrice();
		}
		alipayRequest.setBizContent("{\"out_trade_no\":\"" + orderNo + "\"," + "\"total_amount\":\"" + totalAmount
				+ "\"," + "\"subject\":\"" + subject + "\"," + "\"body\":\"" + body + "\","
				+ "\"timeout_express\":\"90m\"," + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

		/** 转换格式 */
		String result = alipayClient.pageExecute(alipayRequest).getBody().replace('\"', '\'').replace('\n', ' ');
		return result;
	}

}
