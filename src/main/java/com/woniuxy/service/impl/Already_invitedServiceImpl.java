package com.woniuxy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.woniuxy.dao.Already_invitedDao;
import com.woniuxy.entity.Already_invited;
import com.woniuxy.service.Already_invitedService;
/**
 * 
 * @author 阳佳
 *
 */
@Service
public class Already_invitedServiceImpl implements Already_invitedService {
	@Autowired
	private Already_invitedDao already_invitedDao;

	@Override
	public int insert(Already_invited already_invited) {
		return already_invitedDao.insert(already_invited);
	}

	@Override
	public Already_invited findByEmail(String email) {
		return already_invitedDao.findByEmail(email);
	}

	@Override
	public String findEmailByUid(int uid) {
		return already_invitedDao.findEmailByUid(uid);
	}

}
