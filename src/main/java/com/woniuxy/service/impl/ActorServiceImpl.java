package com.woniuxy.service.impl;

import com.woniuxy.dao.ActorDao;
import com.woniuxy.entity.Actor;
import com.woniuxy.service.ActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActorServiceImpl implements ActorService {
    @Autowired
    private ActorDao actorDao;
    @Override
    public Actor getActorById(int actorId) {
        return actorDao.getActorById(actorId);
    }
}
