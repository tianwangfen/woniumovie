package com.woniuxy.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.woniuxy.dao.ShowDao;
import com.woniuxy.entity.Show;
import com.woniuxy.entity.ShowDto;
import com.woniuxy.entity.ShowVo;
import com.woniuxy.service.ShowService;
import com.woniuxy.utils.ResponseEntity;
/**
 * 
 * @author 邹扬
 *
 */
@Service
public class ShowServiceImpl implements ShowService {

	@Autowired
	private ShowDao showDao;

	@Override
	public ShowVo listAll() {
		PageHelper.startPage(1, 5);
		List<Show> list = showDao.listAll();
		PageInfo<Show> pageInfo = new PageInfo<>(list, 5);
		ShowVo showVo = new ShowVo();
		showVo.setPageInfo(pageInfo);
		showVo.setMsg("success");
		showVo.setCode(200);
		return showVo;
	}

	@Override
	public ShowVo ListByCondition(ShowDto showDto) {
		List<Show> list = showDao.ListByCondition(showDto);
		PageInfo<Show> pageInfo = new PageInfo<>(list, 5);
		ShowVo showVo = new ShowVo();
		try {
			BeanUtils.copyProperties(showVo, showDto);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		showVo.setPageInfo(pageInfo);
		return showVo;
	}

	@Override
	public int insertShow(Show show) {
		return showDao.insertShow(show);
	}

	@Override
	public int batchDeleteShows(List<Integer> ids) {
		return showDao.batchDeleteShows(ids);
	}

	@Override
	public int updateShow(Show show) {
		return showDao.updateShow(show);
	}

	@Override
	public Show getShowById(Integer id) {

		return showDao.getShowById(id);
	}

	@Override
	public List<Show> listALL() {
		return showDao.listAll();
	}

	@Override
	public List<Show> getShowByCinemaId(Integer cid) {
		return showDao.getShowByCinemaId(cid);
	}

}
