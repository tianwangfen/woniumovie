package com.woniuxy.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.woniuxy.dao.PackagesDao;
import com.woniuxy.entity.Packages;
import com.woniuxy.service.PackagesService;

import lombok.Data;

@Service
@Data
public class PackagesServiceImpl implements PackagesService{
	
	@Autowired
	PackagesDao packagesDao;
	@Override
	public List<Packages> listAll() {
		// TODO Auto-generated method stub
		return packagesDao.listAll();
	}
	@Override
	public int del(int packageId) {
		//根据packageId查询影院表，若不为空则可以删除套餐
		
		return packagesDao.del(packageId);
	}

}
