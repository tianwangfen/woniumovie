package com.woniuxy.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.woniuxy.dao.Record_VoucherDao;
import com.woniuxy.entity.Record_Voucher;
import com.woniuxy.entity.Voucher;
import com.woniuxy.service.Record_VoucherService;
/**
 * 
 * @author 阳佳
 *
 */
@Service
public class Record_VocherServiceImpl implements Record_VoucherService {
	@Autowired
	private Record_VoucherDao record_VoucherDao;

	@Override
	public int insertRecord(Record_Voucher record_Voucher) {
		int res = record_VoucherDao.insertRecord(record_Voucher);
		return res;
	}

	@Override
	public Record_Voucher findByEmail(String email) {
		return record_VoucherDao.findByEmail(email);
	}

	@Override
	public List<Voucher> findRecord_VoucherByUid(int uid) {
		return record_VoucherDao.findRecord_VoucherByUid(uid);
	}

}
