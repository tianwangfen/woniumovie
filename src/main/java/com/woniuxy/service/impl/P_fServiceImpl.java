package com.woniuxy.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.woniuxy.dao.P_fDao;
import com.woniuxy.entity.P_fBean;
import com.woniuxy.service.P_fService;

import lombok.Data;
@Service
@Data
public class P_fServiceImpl implements P_fService{
	@Autowired
	P_fDao p_fDao;

	@Override
	public List<P_fBean> allP_fBean(int packageId) {
		return p_fDao.allP_fBean(packageId);
	}

	@Override
	public int del(int packageId) {
		// TODO Auto-generated method stub
		return p_fDao.del(packageId);
	}

}
