package com.woniuxy.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.woniuxy.utils.ResponseEntity;
/**
 * 
 * @author 阳佳
 *
 */
@Service
public class PicUploadServiceImpl {
	// 允许上传格式
	private static final String[] IMAGE_TYPE = new String[] { ".bmp", ".jpg", ".jpeg", ".gif", ".png" };

	public ResponseEntity<String> upLoad(MultipartFile uploadFile, HttpServletRequest request) {
		// 图片做校验 对后缀名最校验
		boolean isLeagal = false;
		for (String type : IMAGE_TYPE) {
			if (StringUtils.endsWithIgnoreCase(uploadFile.getOriginalFilename(), type)) {
				isLeagal = true;
				break;
			}
			if (!isLeagal) {// 文件格式不正确
				return new ResponseEntity<String>("文件格式不正确");
			}
		}
		// 获取文件名
		String fileName = uploadFile.getOriginalFilename();

		// 要处理文件名，避免用户上传相同类型、名字的文件出现覆盖情况
		int index = fileName.lastIndexOf(".");
		String type = fileName.substring(index);
		fileName = UUID.randomUUID().toString() + type;
		// 获取存放文件的路径
		String path = request.getServletContext().getRealPath("");// 当前项目的根路径

		File root = new File(path);// 当前项目的根路径

		File webapps = root.getParentFile();// 得到webapps

		// 得到存放文件的目录
		File fileDir = new File(webapps, "files");
		if (!fileDir.exists()) {
			fileDir.mkdirs(); // 创建
		}

		// 得到文件存储的路径 files/文件名
		fileName = fileDir.getAbsolutePath() + File.separator + fileName;

		File destFile = new File(fileName);

		try {
			uploadFile.transferTo(destFile);// 写入磁盘
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<String>("");
	}

}
