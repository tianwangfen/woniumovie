package com.woniuxy.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.woniuxy.dao.MovieDao;
import com.woniuxy.dao.OrderDao;
import com.woniuxy.entity.Movie;
import com.woniuxy.entity.OrderDTO;
import com.woniuxy.entity.OrderPO;
import com.woniuxy.entity.OrderVO;
import com.woniuxy.service.OrderDetailsService;
import com.woniuxy.service.OrderService;
import com.woniuxy.utils.RedisUtil;

import lombok.Data;

/**
 * 订单服务实现类
 * 
 * @author 余枭春
 *
 */
@Service
@Data
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderDao orderDao;
	@Autowired
	private MovieDao movieDao;
	@Autowired
	private OrderDetailsService orderDetailsService;
	@Autowired
	private RedisUtil redisUtil;

	@Override
	public List<OrderPO> listOrderByUserId(Integer userId) {
		return orderDao.listOrderByUserId(userId);
	}

	@Override
	public int insertOrder(OrderDTO order) {
		//用redis存 将订单编号存在redis中
		redisUtil.set("DD"+order.getOrderNo(),order.getOrderNo(),600);
		
		//用数据库
		// 创建订单明细
//		int res = orderDao.insertOrder(order);
//		if (res > 0) {
			try {
				OrderVO order1 = orderDao.getOrderByOrderNo(order.getOrderNo());
				order.setId(order1.getId());
				if (order.getMovieName() == null) {
					Movie movie = movieDao.getMovieByMovieId(order.getMovieId());
					order.setMovieName(movie.getMovieName());
				}
				orderDetailsService.insertOrderDetailsWithOrder(order);
			} catch (Exception e) {
				throw new RuntimeException("生成订单失败");
			}
			return 1;
//		} else {
//			return 0;
//		}
	}

	@Override
	public OrderVO getOrderByOrderNo(String orderNo) {
		return orderDao.getOrderByOrderNo(orderNo);
	}

	@Override
	public int updateOrderStatusByOrderNo(String status, String orderNo) {
		return orderDao.updateOrderStatusByOrderNo(status, orderNo);
	}

	@Override
	public OrderVO getOrderByOrderId(Integer orderId) {
		return orderDao.getOrderByOrderId(orderId);
	}

	@Override
	public int updateOrder(OrderPO order) {
		return orderDao.updateOrder(order);
	}

}
