package com.woniuxy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.woniuxy.dao.UserDao;
import com.woniuxy.entity.UserPO;
import com.woniuxy.service.UserService;

import lombok.Data;
/**
 * 
 * @author 阳佳
 *
 */
@Service
@Data
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao userDao;

	@Override
	public int insertUser(UserPO userPO) {
		int result = 0;
		if (userDao.findUserNumByAccount(userPO.getAccount()) == 0) {
			result = userDao.insertUser(userPO);
		}
		return result;
	}

	@Override
	public UserPO findUserPOByAccount(String account) {
		return userDao.findUserPOByAccount(account);
	}

	@Override
	public UserPO findUserPOByPhoneNum(String phoneNum) {
		return userDao.findUserPOByPhoneNum(phoneNum);
	}

	@Override
	public UserPO findUserPOByEmail(String email) {
		return userDao.findUserPOByEmail(email);
	}

	@Override
	public int resetPassword(String password, String phoneNum) {
		return userDao.resetPassword(password, phoneNum);
	}

	@Override
	public int updateUserInfoByUid(UserPO userPO, int uid) {
		return userDao.updateUserInfoByUid(userPO, uid);
	}

	@Override
	public UserPO findUserPOByUid(int uid) {
		return userDao.findUserPOByUid(uid);
	}

}
