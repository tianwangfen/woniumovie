package com.woniuxy.service.impl;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloopen.rest.sdk.CCPRestSmsSDK;
import com.woniuxy.service.TextService;
import com.woniuxy.utils.RedisUtil;

import lombok.Data;
/**
 * 
 * @author 阳佳
 *
 */
@Service
@Data
public class TextServiceImpl implements TextService {
	@Autowired
	private RedisUtil redisUtil;

	@Override
	public void send(String to, String templateId, String[] datas) throws Exception {
		CCPRestSmsSDK sdk = new CCPRestSmsSDK();
		sdk.init("app.cloopen.com", "8883");
		sdk.setAccount("8a216da87291bbcd01729c5c490e058f", "a22e2980c69e4deb8379574e44616359");
		sdk.setAppId("8a216da87291bbcd01729c5c4a100595");
		HashMap result = sdk.sendTemplateSMS(to, templateId, datas);
		if ("000000".equals(result.get("statusCode"))) {
			System.out.println("短信发送成功");
		} else {
			throw new Exception(result.get("statusCode").toString() + "," + result.get("statusMsg").toString());
		}
	}

	@Override
	public boolean validateText(String phoneNum, String code) {
		// 1.对比验证码
		String key = "phoneNum:" + phoneNum;
		String value = (String) redisUtil.get(key);
		if (value.equals(code) && value != null) {
			return true;
		}
		return false;
	}

}
