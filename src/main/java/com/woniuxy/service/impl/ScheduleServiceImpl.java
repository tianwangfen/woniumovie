package com.woniuxy.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.woniuxy.dao.MovieDao;
import com.woniuxy.dao.ScheduleDao;
import com.woniuxy.entity.Schedule;
import com.woniuxy.service.ScheduleService;

import lombok.Data;

/**
 * 
 * @author 余枭春
 *
 */
@Service
@Data
public class ScheduleServiceImpl implements ScheduleService {
	@Autowired
	private ScheduleDao scheduleDao;
	@Autowired
	private MovieDao movieDao;

	@Override
	public int insertSchedule(Schedule schedule) {

		return scheduleDao.insertSchedule(schedule);
	}

	@Override
	public int updateSchedule(Schedule schedule) {

		return scheduleDao.updateSchedule(schedule);
	}

	@Override
	public List<Schedule> listSchedule(Schedule schedule) {
		List<Schedule> ss = scheduleDao.listSchedule(schedule);
//		for(int i=0;i<ss.size();i++) {
//			ss.get(i).setMovie(movieDao.getMovieByMovieId(ss.get(i).getMovieId()));
//		}
		return ss;
	}

	@Override
	public int deleteSchedule(Integer scheduleId) {
		return scheduleDao.deleteSchedule(scheduleId);
	}

	@Override
	public Schedule getSchedule(Integer scheduleId) {
		return scheduleDao.getScheduleByScheduleId(scheduleId);
	}

	@Override
	public Integer getMaxMovieSessionIdByMovieId(Integer movieId) {
		Integer res = scheduleDao.getMaxMovieSessionIdByMovieId(movieId);
		if (res == null) {
			return 0;
		} else {
			return scheduleDao.getMaxMovieSessionIdByMovieId(movieId);
		}
	}

	@Override
	public String getLastEndTimeWithBeginDate(String beginDate,Integer hallId) {
		return scheduleDao.getLastEndTimeWithBeginDate(beginDate,hallId);
	}

	@Override
	public String getExLastEndTimeWithBeginDate(String beginDate, Integer hallId, String beginTime) {
		
		return scheduleDao.getExLastEndTimeWithBeginDate(beginDate,hallId,beginTime);
	}
}
