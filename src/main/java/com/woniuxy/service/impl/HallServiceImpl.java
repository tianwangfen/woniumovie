package com.woniuxy.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.woniuxy.dao.HallDao;
import com.woniuxy.entity.Hall;
import com.woniuxy.service.HallService;

import lombok.Data;
/**
 * 
 * @author 余枭春
 *
 */
@Service
@Data
public class HallServiceImpl implements HallService {
	@Autowired
	private HallDao hallDao;

	@Override
	public int insertHall(Hall hall) {
		return hallDao.addHall(hall);
	}

	@Override
	public int updateHall(Hall hall) {
		return hallDao.updateHall(hall);
	}

	@Override
	public int deleteHallByHallId(Integer hallId) {
		return hallDao.deleteHallByHallId(hallId);
	}

	@Override
	public List<Hall> listHall(Hall hall) {
		return hallDao.listHall(hall);
	}

	@Override
	public Hall getHallByHallId(Integer hallId) {
		return hallDao.getHallByHallId(hallId);
	}

	@Override
	public Hall getHallByShowIdAndName(Integer showId, String hallName) {
		
		return hallDao.getHallByShowIdAndName(showId,hallName);
	}

	@Override
	public List<Hall> getHallsByShowId(Integer showId) {
		
		return hallDao.getHallsByShowId(showId);
	}

}
