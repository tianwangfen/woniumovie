package com.woniuxy.service.impl;

import com.woniuxy.dao.MovieTypeDao;
import com.woniuxy.entity.MovieType;
import com.woniuxy.service.MovieTypeService;

import lombok.Data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @program: WoniuMovie
 * @description: 电影类别业务类
 * @author: crowned
 * @create: 2020-06-12 18:22
 **/
@Service
@Data
public class MovieTypeServiceImpl implements MovieTypeService {
	@Autowired
	private MovieTypeDao movieTypeDao;

	@Override
	public MovieType getBymovieTypeId(int movieTypeId) {
		return movieTypeDao.getBymovieTypeId(movieTypeId);
	}
}
