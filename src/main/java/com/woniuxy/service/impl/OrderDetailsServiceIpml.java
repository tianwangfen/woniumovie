package com.woniuxy.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.woniuxy.dao.OrderDetailsDao;
import com.woniuxy.entity.Hall;
import com.woniuxy.entity.OrderDTO;
import com.woniuxy.entity.OrderDetails;
import com.woniuxy.entity.Seat;
import com.woniuxy.entity.Show;
import com.woniuxy.service.HallService;
import com.woniuxy.service.OrderDetailsService;
import com.woniuxy.service.ShowService;
import com.woniuxy.utils.OrderUtils;

import lombok.Data;

/**
 * 
 * @author 余枭春
 *
 */
@Service
@Data
public class OrderDetailsServiceIpml implements OrderDetailsService {

	@Autowired
	private OrderDetailsDao orderDetailsDao;

	@Override
	public List<OrderDetails> listDetailsByOrderId(Integer orderId) {
		return orderDetailsDao.listDetailsByOrderId(orderId);
	}

	@Override
	public int insertOrderDetails(OrderDetails orderDetails) {
		return orderDetailsDao.insertOrderDetails(orderDetails);
	}

	@Override
	public int insertOrderDetailsWithOrder(OrderDTO order) {
		try {
			// 先将座位号反序列化
			List<Seat> seats = OrderUtils.getSeats(order.getSeatsInfo());
			for (int i = 0; i < seats.size(); i++) {
				Seat seat = seats.get(i);
				OrderDetails orderDetails = new OrderDetails();
				orderDetails.setCol(seat.getCol());
				orderDetails.setRow(seat.getRow());
				orderDetails.setOrderId(order.getId());
				orderDetails.setMovieName(order.getMovieName());
				orderDetails.setOrderNo(order.getOrderNo());
				orderDetails.setPrice(order.getTotalMoney().divide(new BigDecimal(order.getNums())));
				orderDetails.setPlace(getPlace(order));
				orderDetailsDao.insertOrderDetails(orderDetails);
			}
			return seats.size();
		} catch (Exception e) {
			return -1;
		}
	}

	@Autowired
	private HallService hallService;
	@Autowired
	private ShowService showService;

	private String getPlace(OrderDTO order) {
		Show show = showService.getShowById(order.getShowId());
		Hall hall = hallService.getHallByHallId(order.getHallId());
		String place = "蜗牛影院-" + show.getShowName() + "-" + hall.getHallName();
		return place;
	}

	@Override
	public List<OrderDetails> listDetailsByOrderNo(String orderNo) {
		return orderDetailsDao.listDetailsByOrderNo(orderNo);
	}

}
