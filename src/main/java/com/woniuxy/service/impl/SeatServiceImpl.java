package com.woniuxy.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.woniuxy.dao.SeatDao;
import com.woniuxy.entity.Seat;
import com.woniuxy.service.SeatService;

import lombok.Data;

@Service
@Data
public class SeatServiceImpl implements SeatService {
	@Autowired
	private SeatDao seatDao;

	@Override
	public List<Seat> listSeatsInfoOfHallByScheduleIdId(Integer scheduleId) {
		return seatDao.listSeatsInfoOfHallByScheduleIdId(scheduleId);
	}
}
