package com.woniuxy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.woniuxy.dao.ManagerDao;
import com.woniuxy.service.ManagerService;

@Service
public class ManagerServiceImpl implements ManagerService{
	@Autowired
	ManagerDao managerDao;

	@Override
	public String selectManagerByAccount(String managerAccount) {
		// TODO Auto-generated method stub
		return managerDao.selectManagerByAccount(managerAccount);
	}

	@Override
	public String selctpwdByAccount(String managerAccount) {
		// TODO Auto-generated method stub
		return managerDao.selctpwdByAccount(managerAccount);
	}

}
