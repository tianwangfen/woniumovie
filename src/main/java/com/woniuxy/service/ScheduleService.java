package com.woniuxy.service;

import java.util.List;

import com.woniuxy.entity.Schedule;
/**
 * 
 * @author 余枭春
 *
 */
public interface ScheduleService {
	int insertSchedule(Schedule schedule);

	int updateSchedule(Schedule schedule);

	List<Schedule> listSchedule(Schedule schedule);

	Schedule getSchedule(Integer scheduleId);

	int deleteSchedule(Integer scheduleId);

	Integer getMaxMovieSessionIdByMovieId(Integer movieId);

	String getLastEndTimeWithBeginDate(String beginDate, Integer hallId);

	String getExLastEndTimeWithBeginDate(String beginDate, Integer hallId, String beginTime);
}
