package com.woniuxy.service;

import java.util.List;

import com.woniuxy.entity.OrderDTO;
import com.woniuxy.entity.OrderPO;
import com.woniuxy.entity.OrderVO;

/**
 * 订单服务类
 * 
 * @author 余枭春
 *
 */
public interface OrderService {
	List<OrderPO> listOrderByUserId(Integer userId);

	int insertOrder(OrderDTO order);

	OrderVO getOrderByOrderNo(String orderNo);

	OrderVO getOrderByOrderId(Integer orderId);

	int updateOrderStatusByOrderNo(String status, String orderNo);

	int updateOrder(OrderPO order);
}
