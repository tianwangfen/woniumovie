package com.woniuxy.service;

import java.util.List;

import com.woniuxy.entity.P_function;

public interface P_functionService {
	List<P_function> selectAll();
	
	String deleteFunctionById(int p_functionId);


}
