package com.woniuxy.interceptor;

import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
/**
 * 
 * @author 邹扬
 *
 */
@Service
public class FileInterceptor extends HandlerInterceptorAdapter{
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		boolean flag = true;//标记，文件是否合法	默认合格
		
		//判断是否是文件上的请求
		if(request instanceof MultipartHttpServletRequest) {
			//才应该去判断文件是否合法
			MultipartHttpServletRequest mhr = (MultipartHttpServletRequest)request;
			
			//获取文化map
			Map<String, MultipartFile> files = mhr.getFileMap();
			
			//迭代判断类型
			Iterator<String> iterator = files.keySet().iterator();
			while (iterator.hasNext()) {
				String key = (String)iterator.next();
				MultipartFile file = files.get(key);//得到上传文件
				//得到文件类型
				String type = file.getContentType();
				flag = checkType(type);
			}
		}
		return flag;//返回true表示继续执行，让handler处理请求，否则直接不处理请求
	}
	
	private boolean checkType(String type){
        String types = "image/png,image/jpg,image/jpeg,image/bmp,image/gif";
        return types.contains(type);
    }
}
