package com.woniuxy.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * redis来实现密码错误管理器中的方法
 * 
 * @author 阳佳
 *
 */
@Service
public class ErrorNumManagerUtils implements ErrorNumManager {
	private static final String REDIS_PREFIX = "errorCount:";
	private static final int ErrorMax = 3;
	@Autowired
	private RedisUtil redisUtil;

	@Override
	public int currErrorNum(String account) {
		Integer currErrorNum = (Integer) redisUtil.get(REDIS_PREFIX + account);
		return null == currErrorNum ? 0 : currErrorNum;
	}

	@Override
	public int addErrorNum(String account) {
		Integer currErrorNum = (Integer) redisUtil.get(REDIS_PREFIX + account);
		int errorNum = (null == currErrorNum ? 1 : currErrorNum + 1);
		redisUtil.set(REDIS_PREFIX + account, errorNum);
		return errorNum;
	}

	@Override
	public void delErrorNum(String account) {
		redisUtil.del(REDIS_PREFIX + account);
	}

	@Override
	public boolean reachErrorMax(int currErrorNum) {
		return currErrorNum >= ErrorMax;
	}

}
