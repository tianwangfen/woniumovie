package com.woniuxy.utils;
/**
 * 登录时 密码错误次数管理器
 * @author 阳佳
 *
 */
public interface ErrorNumManager {
	/**
	 * 当前账号输错了几次密码
	 * @param account
	 * @return
	 */
	public int currErrorNum(String account);
	/**
	 * 密码错误次数加1
	 * @param account
	 * @return
	 */
	public int addErrorNum(String account);
	/**
	 * 判断错误次数是否达到3次
	 * @param currErrorNum
	 * @return
	 */
	public boolean reachErrorMax(int currErrorNum);
	/**
	 *密码正确时删错缓存中的错误次数
	 * @param account
	 */
	public void delErrorNum(String account);
	
}
