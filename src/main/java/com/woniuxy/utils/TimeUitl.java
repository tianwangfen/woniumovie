package com.woniuxy.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUitl {
	public static String getTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		return dateFormat.format(new Date());
	}
}
