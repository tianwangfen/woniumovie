package com.woniuxy.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
/**
 * 
 * @author 余枭春
 *
 */
public class AllStatusFormProperties {

	private static final String ORDER_NOTPAYED = "";

	/**
	 * 加载配置类中所有状态的信息，并封装到一个map中，通过键值对去取，暂时没用
	 * 
	 * @return 封装了所有状态的map，通过相应的可以去取，见名思意
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static Map<String, String> allStatus() throws FileNotFoundException, IOException {
		Map<String, String> status = new HashMap<String, String>();
		Properties prep = new Properties();
		prep.load(new FileInputStream(new File("classpath:movieStatus.properties")));
		status.put(ORDER_NOTPAYED, prep.getProperty("ORDER_NOTPAYED"));
		return status;
	}

}
