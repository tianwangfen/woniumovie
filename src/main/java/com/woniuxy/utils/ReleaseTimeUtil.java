package com.woniuxy.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *  @author: crowned
 *  @Date: 2020/6/9 23:51
 *  @Description: 影片发布时间工具类
 */
public class ReleaseTimeUtil {
    public static String getTime(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return dateFormat.format(new Date());
    }
}
