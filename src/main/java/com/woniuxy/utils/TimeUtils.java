package com.woniuxy.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间操作 获取当前时间 增加时间
 * 
 * @author 阳佳
 *
 */
public class TimeUtils {
	/**
	 * 获取时间
	 * 
	 * @return
	 */
	public static String getTime() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		return dateFormat.format(new Date());
	}
	/**
	 * 
	 * @param day 起点时间
	 * @param hour  需要增加的时间
	 * @return
	 */
	public static String addDate(String day, int hour) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = null;
		try {
			date = format.parse(day);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (date == null)
			return "";
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR, hour);// 24小时制
		date = cal.getTime();
		cal = null;
		return format.format(date);

	}


}
