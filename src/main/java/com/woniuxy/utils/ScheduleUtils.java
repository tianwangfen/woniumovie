package com.woniuxy.utils;

import com.woniuxy.entity.Schedule;
/**
 * 
 * @author 余枭春
 *
 */
public class ScheduleUtils {
	/**
	 * 判断字符串是否为空
	 * @param string
	 * @return
	 */
	public static boolean checkIsNotNull(String string) {
		return string != null && string.length() != 0;
	}
	
	/**
	 * 封装了查询redis中座位的key
	 * 
	 * @param schedule
	 * @return
	 */
	public static String getKeyOfSeats(Schedule schedule) {
		String keyOfSeats = schedule.getShowId() + "-" + schedule.getHallId() + "-" + schedule.getMovieId() + "-"
				+ schedule.getMovieSessionId();
		return keyOfSeats;
	}
}
