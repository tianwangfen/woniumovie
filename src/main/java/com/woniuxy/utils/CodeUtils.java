package com.woniuxy.utils;

import java.util.Random;

import org.springframework.boot.SpringApplication;

/*
 * 随机产生6位验证码
 */
/**
 * 
 * @author 阳佳
 *
 */
public class CodeUtils {
	public static int getCode() {
		int max = 999999;
		int min = 111111;
		Random random = new Random();
		return random.nextInt(max) % (max - min + 1) + min;
	}
}
