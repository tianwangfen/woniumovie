package com.woniuxy.utils;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 统一响应格式
 *
 * @author 余枭春
 *
 */

public class ResponseEntity<T> {
	private int code;// 状态码
	private String msg;
	private T data;


	public ResponseEntity() {

	}

	public ResponseEntity(T data) {
		this(200, "ok", data);
	}

	public ResponseEntity(int code, String msg) {
		this(code, msg, null);
	}

	public ResponseEntity(int code, String msg, T data) {
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	public static final ResponseEntity<Void> SUCCESS = new ResponseEntity<Void>(200, "ok");
	public static final ResponseEntity<Void> FAIL = new ResponseEntity<Void>(500, "Error");


	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
