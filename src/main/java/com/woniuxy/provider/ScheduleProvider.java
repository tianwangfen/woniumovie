package com.woniuxy.provider;

import org.apache.ibatis.jdbc.SQL;

import com.woniuxy.entity.Schedule;
import com.woniuxy.utils.ScheduleUtils;

/**
 * 提供动态SQL语句
 * 
 * @author 余枭春
 *
 */
public class ScheduleProvider {
	public String listSchedule(Schedule schedule) {
		SQL sql = new SQL() {
			{
				SELECT("*");
				FROM("schedule");
				if (schedule.getScheduleId() != null) {// 排片序号
					WHERE("scheduleId=" + schedule.getScheduleId());
				}
				if (schedule.getShowId() != null) {// 放映点
					WHERE("showId=" + schedule.getShowId());
				}
				if (schedule.getHallId() != null) {// 影厅
					WHERE("hallId=" + schedule.getHallId());
				}
				if (schedule.getMovieSessionId() != null) {// 电影场次
					WHERE("movieSessionId =" + schedule.getMovieSessionId());
				}
				if (schedule.getDiscount() != null && !schedule.getDiscount().equals(0)) {// 排片序号
					WHERE("discount=" + schedule.getDiscount());// 折扣
				}
				if (schedule.getMovieId() != null) {
					WHERE("movieId=" + schedule.getMovieId());// 电影名
				}
				if (schedule.getBeginDate() != null && schedule.getBeginDate().length() != 0) {
					WHERE("beginDate='" + schedule.getBeginDate() + "'");// 放映日期
				}
				if (ScheduleUtils.checkIsNotNull(schedule.getBeginTime())) {
					WHERE("beginTime>='" + schedule.getBeginTime() + "'");// 开始时间
				}
				if (ScheduleUtils.checkIsNotNull(schedule.getEndTime())) {
					WHERE("endTime<='" + schedule.getEndTime() + "'");// 结束时间
				}
			}
		};
		return sql.toString();
	}
}
