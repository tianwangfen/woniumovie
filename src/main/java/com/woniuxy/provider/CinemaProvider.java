package com.woniuxy.provider;

import org.apache.ibatis.jdbc.SQL;

import com.woniuxy.entity.Cinema;
/**
 * 
 * @author 邹扬
 *
 */
public class CinemaProvider {
	public String updateCinema(Cinema cinema) {
		return new SQL() {
			{
				UPDATE("cinema");
				if(cinema.getCinemaName()!=null&&cinema.getCinemaName().trim()!="") {
					SET("cinemaName=#{cinemaName}");
				}
				if(cinema.getLogo()!=null&&cinema.getLogo().trim()!="") {
					SET("logo=#{logo}");
				}
				if(cinema.getEquipment()!=null&&cinema.getEquipment().trim()!="") {
					SET("equipment=#{equipment}");
				}
				WHERE("cinemaId=#{cinemaId}");
			}
		}.toString();
	}
}

