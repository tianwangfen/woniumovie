package com.woniuxy.provider;

import org.apache.ibatis.jdbc.SQL;

import com.woniuxy.entity.Hall;

/**
 * 提供厅类的动态SQL
 * 
 * @author 余枭春
 *
 */
public class HallProvider {
	public String listHall(Hall hall) {
		SQL sql = new SQL() {
			{
				SELECT("*");
				FROM("hall");
				if (hall.getShowId()!= null) {// 放映点
					WHERE("showId=" + hall.getShowId());
				}
				if (hall.getHallName() != null) {// 展厅名字
					WHERE("hallId=" + hall.getHallName());
				}
			}
		};
		return sql.toString();
	}
	
}
