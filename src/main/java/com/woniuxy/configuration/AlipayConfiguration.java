package com.woniuxy.configuration;

import java.io.FileWriter;
import java.io.IOException;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

@EnableCaching
@Configuration
public class AlipayConfiguration {

	// 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
	public static String APP_ID = "2016092100562103";
	
	// 商户私钥，您的PKCS8格式RSA2私钥，这些就是我们刚才设置的
    public static String MERCHANT_PRIVATE_KEY = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCMLz6dlJJlP0ZHhPTCXKP5K2cixOYYLtQx6L1AJea/C6Fg+K+SaMNbGEcDa4WMuQM/9vtxr2Nr5QP6phTp2cU55ZtRijqqa7Y46A1LMjvklRmCXe/qllRtg5SKrtczimETEOMhc/802U3Fbg7wBSxdbO8w+EgTKJYcb/lZfMeRlZmSnE+3jQ1ZjmhC6rKm5xOHTuLkw9LlSmGpHbjonx7c/EzDxs3sMnDg1WvaMwWsxnHDTlw3gf3Uy1xf78CRUz5Li/GYVOxj3PeHzoteHAEyF5EHaVTWu1798dBMlffAD/wkXiYuR4tqufz1X05VNXDPhiEXTNo/qV7fCUDNWu/hAgMBAAECggEAUAzqhLHCp25qg4y4+ARO3oqaapBllImpQpbU+zypd478qJNaLxED/ZDvfFR5/tHnuS8ghtrQu1wb2ZXde2yP42/qMbXtqkO32ucoMDiPWwiJeJiL2DngHGl2+Xq+CQCMTobmLMRAh71PW95tSUKrAu6h6CTfg5E5pyOKED/g9pDKWT04DlDJ05yZUPFya08ttx/LjoEWTpI3m5FYiW5eRoWoYbbNpHh1NCcztbmn0+dy2JvvFTRGpl+8fhzYQIxtWTzghu0/V+KxpX6XtmW1cFFTKDd6sF6fppgw0ZBusq4aYXroXBjBCReu07ogNjrhPAKsf55z+5fJ72R3nTCMwQKBgQDnf4u39aKqfwcZ4hCrZaaH3G3Hmt0wxJYZnK2FmUGUa3QJ14SFzheNWhMkO13+qgCJUYbEqGW5I6whw1d4ZcxeF06yqaRtSXlYajjOYwEut2tpO9zMpzwlWJ5QkqSEJ5vFbPQcsVcnaHwFddixFsTBsV6vmLWYB95yuLSQ4/PnTwKBgQCbBY0G4NQ4y5l+4X1Nbv2cXrhfmJ4JOwXjQv3oe6URQCTjwwCd9SErrRcoctoUKU3L/Mmeck1wZf8kFHyMQ8rbkYdiliks30XlU1KOUlpUroE6Vgm4nhXvg3P0VOnl1qSAKMRDxrMefxApxDK/78fBBSxPcLm11hWHTG4IAaTpzwKBgBoPv9j77E4pctE1JA9HqFHPPQSlqJUe7I8Bln9d24Yr+qpI+wBdTUV6iIEhtdk/pkPPS87D7n/DRlOevDAvtaw6D4AJnRs4lFKh0rdZaf6qY4s7VAYuJZVGvrvvZloCY4T8WRcJ1/f1yVCMDdMR7yCnKTO3i2oMKLzdkmZlY0X9AoGATK3tOSbCsqk67sDYuxGYjt1nc5nJKiWw3t4HPBTU7Hh/Bs4xIvDajIEcwFw3NNvNaIZMsHw3g85f6tfbV4I8+Gi+v+wox9JU/388ibuAhPygXVuriU9g6E1scMIDxDVsvxvrIN9LQF13gEFVHQ2dEAPgDBdlNOWFVpxenMU7bo8CgYEAykc27kmYWs3aGPzCv6KxCRrhIpuWbyuKYbgNXU2Mz/kH/eCXRqG8/DKRhYsVYGoJPFkWsS1LTb9cPKlCK94bO7Ge39+nuvkbuUSeRTz5j0GlqaTefIz4TM1x7L+3C57L5FIa8rUP0Nwf2QquGISXktszqTPbxo4qJ3z95+Y6vWQ=\r\n";
	
	// 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。，这些就是我们刚才设置的
    public static String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgWAE4YUhH75N3fsmDZewYM4CPOJ6ntAVt/ujEBIfbq3lblM83VZivWrcjBCU+m6Ps15I5NLzRSNhJWMf1LevWxGUFoYFpbnNY8n99VW2yFioMmQkZ/jJ+xOpBvZaCioP6/N+MaNPystto/8B5fOlxkwd8i3fj9cHSNVFk6fTT7GYTbeEwjrSlhNxJvnpajVkugB52840vP0zBSPeBE/ip9102gWpCiYTkaFpf80WbHtj140kcAMzKzLlLHvmLc2hYdCcGyCVcca9qELSdOOPXuwe1HK+MYj1BMNZM0VljeTQQWZAXbg8sMRPLR1UiuXq0yFMMsnAQzqttuWppl+zXwIDAQAB\r\n";

    //异步通知，再这里我们设计自己的后台代码
    public static String notify_url = "http://3122086we0.zicp.vip:28672/woniushop/alipay/notify";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://3122086we0.zicp.vip:28672/woniushop/alipay/return";

	// 签名方式
	public static String SIGN_TYPE = "RSA2";
	
	// 字符编码格式
	public static String CHARSET = "utf-8";
	
	// 支付宝网关
	public static String GATEWAYURL = "https://openapi.alipay.com/gateway.do";
	
	// 支付宝网关
	public static String LOG_PATH = "D:\\";


	  /** 
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(LOG_PATH + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
