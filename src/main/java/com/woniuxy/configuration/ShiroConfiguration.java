package com.woniuxy.configuration;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import com.woniuxy.realm.UserRealm;
/**
 * 
 * @author 阳佳
 *
 */
@Configuration
public class ShiroConfiguration {
	// realm
	@Bean // 默认将方法名作为id
	public UserRealm realm() {
		UserRealm realm = new UserRealm();
		// 开启缓存
		// realm.setCachingEnabled(true);
		// 设置认证缓存
		realm.setAuthenticationCachingEnabled(true);
		realm.setAuthenticationCacheName("abc");
		// 设置授权的缓存
		realm.setAuthorizationCachingEnabled(true);
		realm.setAuthorizationCacheName("cba");

		return realm;
	}

	// securityManager
	@Bean // 参数的名字要与上边方法的名字保持一致
	public SecurityManager securityManager(UserRealm realm) {
		DefaultWebSecurityManager manager = new DefaultWebSecurityManager();
		// 设置realm
		manager.setRealm(realm);

		// 直接调方法获取缓存管理器
		// manager.setCacheManager(ehCacheManager());
		// 设置session管理器
		manager.setSessionManager(sessionManager());
		return manager;
	}

	// 过滤器
	@Bean
	public ShiroFilterFactoryBean shiroFilter(SecurityManager securityManager) {
		ShiroFilterFactoryBean factoryBean = new ShiroFilterFactoryBean();
		// 设置安全管理器
		factoryBean.setSecurityManager(securityManager);
		// 登录页面
		factoryBean.setLoginUrl("/loginByAccount.html");
		// 没权限时显示的页面
		factoryBean.setUnauthorizedUrl("/error.html");
		// 设置过滤器链
		Map<String, String> map = new LinkedHashMap<>(); // 有序
		// 设置
		map.put("/woniuuser/register.html", "anon");
		map.put("/css/**", "anon");
		map.put("/js/**", "anon");
		map.put("/vue/**", "anon");
		map.put("/image/**", "anon");
		map.put("/html/**", "anon");
		map.put("/loginByAccount.html", "anon");
		map.put("/loginByPhoneNum.html", "anon");
		map.put("/logout", "logout");
		map.put("/text/send", "anon");
		map.put("/user/register", "anon");
		map.put("/user/loginByAccount", "anon");
		map.put("/user/loginByPhoneNum", "anon");
		map.put("/user/forgetPasswordSecond", "anon");
		map.put("/user/forgetPasswordFirst", "anon");
		map.put("/forgetPasswordFirst.html", "anon");
		map.put("/forgetPasswordSecond.html", "anon");
		map.put("/captcha", "anon");
		map.put("/**", "anon");

		// 设置
		factoryBean.setFilterChainDefinitionMap(map);

		return factoryBean;
	}

	/*
	 * 注解
	 */
	// 生命周期管理器
	@Bean
	public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
		return new LifecycleBeanPostProcessor();
	}

	@Bean
	@DependsOn("lifecycleBeanPostProcessor")
	public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
		DefaultAdvisorAutoProxyCreator creator = new DefaultAdvisorAutoProxyCreator();
		creator.setProxyTargetClass(true);
		return creator;
	}

	@Bean
	public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
		AuthorizationAttributeSourceAdvisor advisor = new AuthorizationAttributeSourceAdvisor();
		advisor.setSecurityManager(securityManager);
		return advisor;
	}

	// 设置shiro session过期时间
	@Bean(name = "sessionmanager")
	public DefaultWebSessionManager sessionManager() {
		DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
		// 负数表示永不过期
		sessionManager.setGlobalSessionTimeout(-1000L);
		return sessionManager;
	}

}

