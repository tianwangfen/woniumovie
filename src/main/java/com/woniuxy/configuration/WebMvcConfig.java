package com.woniuxy.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.HttpPutFormContentFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.woniuxy.interceptor.FileInterceptor;
/**
 * 
 * @author 邹扬
 *
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
	@Autowired
	private FileInterceptor fileInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(fileInterceptor).addPathPatterns("/**");
	}
	 @Bean
	    public HttpPutFormContentFilter httpPutFormContentFilter() {
	        return new HttpPutFormContentFilter();
	    }

}
