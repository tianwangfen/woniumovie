package com.woniuxy.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;

import com.woniuxy.controller.OrderController;
import com.woniuxy.service.OrderService;
import com.woniuxy.service.VoucherService;

/**
 * 处理redis中各种失效的key 每种key失效后 处理方法不同
 * 
 * @author 阳佳
 *
 */
public class RedisKeyExpirationListener extends KeyExpirationEventMessageListener {
	@Autowired
	private VoucherService voucherService;
	@Autowired
	private OrderService orderService;
	// 定义代金券状态
	private static final int used = 1;// 已使用
	private static final int unUsed = 2;// 未使用
	private static final int using = 3;// 占用

	public RedisKeyExpirationListener(RedisMessageListenerContainer listenerContainer) {
		super(listenerContainer);
	}

	/**
	 * 针对redis数据失效事件，进行数据处理
	 *
	 * @param message
	 * @param pattern
	 */
	@Override
	public void onMessage(Message message, byte[] pattern) {
		// message为失效的key值
		String expiredKey = message.toString();
		// 图形验证码
		if (expiredKey.startsWith("captcha")) {
			return;
		}
		// 短信验证码
		if (expiredKey.startsWith("phoneNum")) {
			return;
		}
		// 订单号 订单过期 修改订单状态  状态5表示订单失效
		if (expiredKey.startsWith("DD")) {
			orderService.updateOrderStatusByOrderNo(OrderController.ORDER_FAIL, expiredKey.substring(2));
		}
		// 优惠券
		if (expiredKey.startsWith("vid")) {
			// 得到vid
			String vid = expiredKey.substring(4);
			System.out.println(vid);
			// 用失效的key(vid) 去查询当前代金券状态
			int status = voucherService.getCurrentStatus(Integer.valueOf(vid));
			// key失效时 判断失效时voucher的状态 如果status未使用 就将它过期
			if (status == unUsed) {
				voucherService.updateStatus(Integer.valueOf(vid));
			}
		}

	}

}
