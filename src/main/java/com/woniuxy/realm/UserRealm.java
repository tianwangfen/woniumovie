package com.woniuxy.realm;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import com.woniuxy.entity.UserPO;
import com.woniuxy.service.UserService;
/**
 * 
 * @author 阳佳
 *
 */
public class UserRealm extends AuthorizingRealm{
	@Autowired
	private UserService userService;
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		// 1.获取前端传入的账号
		String account=token.getPrincipal().toString();
		// 2,在数据库进行查询
		UserPO userPO=userService.findUserPOByAccount(account);
		if (userPO!=null) {
			SimpleAuthenticationInfo info=new SimpleAuthenticationInfo(userPO.getAccount(),userPO.getPassword(),getName());
			return info;
		}
		return null;
	}

}
