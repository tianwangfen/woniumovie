function axiosGet(URL,params) {
    return axios.get(
        URL,
        params
    );
}

function axiosPost(URL, params) {
    return axios.post(
        URL,
        params
    );
}

function showMovieNames(obj,data){
    var movieNames=[];
    for(let i=data.length-1;i>=0;i--){
        movieNames.push(data[i].movieName);
    }
    
    $.each(obj,function(i,o){
        $(o).html(""+movieNames[i]);
    })
    
}


function getMovieIds(obj,data){
    var movieIds=[];
    for (i in data){
        movieIds[movieIds.length]=data[i].movieId
    }
    movieIds=movieIds.reverse()
    $.each(obj,function(i,o){
        $(o).prop("href","/movie/"+movieIds[i])
    })  
}

// function Link(obj,data){
//     var index = data.length-1;
//     $.map(res.data, function (o, index) {
//         console.log(o.movieName);
//         console.log()
//     });
// }
// function getData(data,ele){
//     var index = res.data.length-1;
//     $.map(res.data, function (o, i) {
//         ele.html(""+data[index])                
//         index--;
//     });
// }