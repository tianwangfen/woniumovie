
function axiosGet(URL) {
	return axios.get(
		URL,
	);
}
function axiosPost(URL, params) {
	return axios.post(
		URL,
		params
	);
}

function getCinemas(vue){
	axiosGet("/cinema/all").then(res=>{
		vue.cinemaOptions=res.data.data;
	})
}

function getTableWhenMounted(vue){
	axiosGet("/show/all").then(res => {
				vue.totalPage=res.data.pageInfo.pages;
				vue.total=res.data.pageInfo.total;
				/*表格数据*/
				var list = res.data.pageInfo.list;
				console.log(list);
				vue.tableData= list;
				list.forEach(item=>{
					if(item.flag==1){
						item.flag="存在"
					}
					if(item.flag==0){
						item.flag="不存在"
					}
				});
		});
}

function getPronviceCityAreaData(vue){
	axiosGet("/elementui/ChinaCity.json")
				.then(res=>{
					console.log(res.data);
					vue.provinceData=res.data;
					vue.addrConditionOptions=res.data;
				}).catch(e => {
					vue.$message.error("网络连接超时");
			    })
}

function emptyTheCondition(vue){
			this.name="";
    	    this.address="";
    	    this.flag="";
}

function tableListByPageCondition(vue,params){
	axiosPost("/show/list",params).then(res=>{
	    	  			    vue.totalPage=res.data.pageInfo.pages;
	    	  			    vue.total=res.data.pageInfo.total;
	    	  			    	  		
	    	  			    	list = res.data.pageInfo.list;
	    	  			    	vue.tableData= list;
	    	  			    	list.forEach(item=>{
	    	  			    	  	if(item.flag==1){
	    	  			    	  		item.flag="存在"
	    	  			    	  }
	    	  			    	  	if(item.flag==0){
	    	  			    	  		item.flag="不存在"
	    	  			    	}
	    	  			 })
	    	  })
}

function turnPage(vue,val,params){
	vue.currentPage=val;
   	emptyTheCondition(vue);
	    	  		params.append("pageNum",val);
	    	  		params.append("pageSize",vue.pageSize);
	    	  		params.append("showName",vue.nameCondition);
	    	  		params.append("address",vue.addressCondition);
	    	  		params.append("flag",vue.isExistCondition);
	tableListByPageCondition(vue,params);
}

function queryByConditions(vue,params){
	params.append("pageNum",vue.currentPage);
    	    		params.append("pageSize",vue.pageSize);
    	    		params.append("showName",vue.nameCondition=vue.name);
    	    		params.append("address",vue.addressCondition=vue.address);
    	    		params.append("flag",vue.isExistCondition=vue.flag);
	tableListByPageCondition(vue,params)
}

function changePageSize(vue,val,params){
	vue.pageSize=val;
	emptyTheCondition(vue);
					params.append("pageNum",vue.currentPage);
	    	  		params.append("pageSize",val);
	    	  		params.append("showName",vue.nameCondition);
	    	  		params.append("address",vue.addressCondition);
	    	  		params.append("flag",vue.isExistCondition);
	tableListByPageCondition(vue,params);
}

function emptyFormData(vue){
	vue.form.name=""
	vue.form.provinceValue=""
	vue.form.cityValue=""
	vue.form.areaValue=""
	vue.form.detailedAddress=""
	vue.form.textarea=""
	vue.form.imgpath=""
	vue.form.cinema=""
}

function formSubmit(vue,params){
					params.append("showName",vue.form.name);
    				params.append("province",vue.form.provinceValue);
    				params.append("city",vue.form.cityValue);
    				params.append("area",vue.form.areaValue);
    				params.append("site",vue.form.detailedAddress);
    				params.append("description",vue.form.textarea);
    				params.append("image",vue.form.imgpath);
    				params.append("cinemaId",vue.form.cinema);
    				params.append("flag",1);
					axiosPost("/show/add",params).then(res=>{
    				if(res.data.code=200){
    					    alert("成功")
    						emptyFormData(vue)
							notification(vue)
							vue.dialogFormVisible=false
    					}else{
    						alert("系统繁忙")
    					}
    				})
}

function notification(vue){
        const h = vue.$createElement;

        vue.$notify({
          title: '标题名称',
          message: h('i', { style: 'color: teal'}, '添加成功')
        });
      
}