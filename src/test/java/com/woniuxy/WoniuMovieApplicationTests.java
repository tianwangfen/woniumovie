package com.woniuxy;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.woniuxy.dao.ShowDao;
import com.woniuxy.entity.Show;
import com.woniuxy.entity.ShowDto;
import com.woniuxy.service.ShowService;



@SpringBootTest
class WoniuMovieApplicationTests {
	
	@Autowired
	private ShowDao showDao;
	
	@Test
	void contextLoads() {
		System.out.println(showDao.getShowByCinemaId(2));
		
	}

}
